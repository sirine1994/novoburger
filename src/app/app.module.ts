import { ErrorHandler, NgModule } from '@angular/core';
import {BrowserModule, SafeHtml} from '@angular/platform-browser';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Stripe } from '@ionic-native/stripe';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { DatePicker } from '@ionic-native/date-picker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CurrencyProvider } from '../providers/currency';
import { SearchPage } from '../pages/search/search';
import { CommandesClientProvider } from '../providers/commandes-client/commandes-client';
import { UserProvider } from '../providers/user/user';
import { AdministrationProvider } from '../providers/administration/administration';
import { BoCommandesProvider } from '../providers/bo-commandes/bo-commandes';
import { NetworkProvider } from '../providers/network/network';
import { FcmProvider } from '../providers/fcm/fcm';
import { FCM } from '@ionic-native/fcm';
import {NativeGeocoder} from '@ionic-native/native-geocoder';
import {GooglePlus} from "@ionic-native/google-plus";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicStorageModule.forRoot(),
  IonicModule.forRoot(MyApp, {
            preloadModules: true,

            scrollAssist: true,

            autoFocusAssist: true,
            platforms: {
                ios: {
                    mode: 'md'
                }
            }
        }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      } })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    CurrencyProvider,
    StatusBar,
    SplashScreen,
    Stripe,
    Facebook,
    SocialSharing,
GooglePlus,
    InAppBrowser,
    CallNumber,

      NativeGeocoder,
    Facebook,
    HttpModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommandesClientProvider,
    UserProvider,
    DatePicker,
    AdministrationProvider,
    BoCommandesProvider,
    NetworkProvider,
    FcmProvider,
    FCM,
  ]
})
export class AppModule {}
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}