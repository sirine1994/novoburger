import {Component, ViewChild, NgModule} from "@angular/core";
import {Nav, Platform, App, LoadingController, NavController, AlertController, MenuController} from "ionic-angular";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {Http, Headers} from "@angular/http";
import * as Constant from "../config/constants";
import {Storage} from "@ionic/storage";
import {TranslateService} from "@ngx-translate/core";
import {Events} from "ionic-angular";
import {CurrencyProvider} from "../providers/currency";
import {UserProvider} from "../providers/user/user";
import {FcmProvider} from "../providers/fcm/fcm";
import {StoreCardPage} from "../pages/store-card/store-card";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {AproposPage} from "../pages/apropos/apropos";
import {FaqPage} from "../pages/faq/faq";
import {CouponFidelitePage} from "../pages/coupon-fidelite/coupon-fidelite";
import {ListCardPage} from "../pages/list-card/list-card";
import {searchStorePage} from "../pages/searchStore/searchStore";
import {RestGlobals} from "../providers/rest-globals";
 

@Component({
    templateUrl: "app.html"
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any /* = 'LoginPage'*/;

    base_url: any;
    total_card:any=0;
    user: any;
    noConnect:boolean=false;
    list: any = [];
    pages: Array<{ title: string; component: any; }>;
    root_pages_admin: Array<{ title: string; component: any; }>;
    root_pages_vendeur: Array<{ title: string; component: any;}>;
    client_pages: Array<{ title: string; component: any;}>;
    isClient:boolean=false;
    constructor(public menuCtrl: MenuController,
                public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                public http: Http,
                public app: App,
                public events: Events,
                public storage: Storage,
                public iab: InAppBrowser,
                public currencyProvider: CurrencyProvider,
                public translate: TranslateService,
                public fcmService: FcmProvider,
                public userService: UserProvider,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController) {
        translate.setDefaultLang("en");
        translate.use('en');
        this.storage.ready().then(() => {
            this.http
                .get(this.base_url + "api/settings_api/settings")
                .subscribe(data => {
                    this.storage.set("settings", data.json());
                    this.events.publish("settings: done", data.json());
                    this.currencyProvider;
                });
        });
        events.subscribe("user:add_cart", (user, time) => {
            this.total_card += 1;
        });

        this.base_url = Constant.domainConfig.base_url;
        // src\pages\commandes
        this.client_pages = [
            /*{title: "Mes commandes", component: "CategoriesPage", icon: "menuu"},*/
            {title: "Restaurants", component: "searchStorePage" },
            {title: "Mes commandes", component: "CommandesClientPage"},
            {title: "Mes informations", component: "ProfilePage"},
            {title: "Moyens de paiement", component: "StoreCardPage"},
            {title: "Adresses enregistrées", component: "AddressPage"},
/*
            {title: "Mes coupons fidélité", component: "CouponFidelitePage"},
*/
            {title: "Mon panier", component: "CartPage"},

            {title: "FAQ", component: "FaqPage"},
            {title: "A propos", component: "AproposPage"}
            /* {title: res.contact, component: "AboutPage", icon: "contactt"}*/
        ]

        this.translate.get(['pending_orders','list_products','order_history','settings']).subscribe(res => {

            this.root_pages_admin = [
                {
                    title: res.pending_orders,
                    component: "CommandeAdminTabsPage",
                },
                {
                    title: res.list_products,
                    component: "CategoriesCommerctPage",
                },
                {
                    title: res.order_history,
                    component: "HistoriqueCommandesPage",
                },
                {title: res.settings, component: "SettingsPage", }
            ];

            this.root_pages_vendeur = [
                {
                    title: res.pending_orders,
                    component: "CommandeAdminTabsPage",
                },
                {
                    title: res.list_products,
                    component: "CategoriesCommerctPage",
                },
                {
                    title: res.order_history,
                    component: "HistoriqueCommandesPage",
                }
            ]});
        this.user = new Array();
        this.events.subscribe("user: change", () => {
            return this.storage.get("user").then(obj => {
                this.user = new Array();
                console.log("chnaged");
                if (obj == null) {
                    this.user = null;
                } else {
                    this.user = obj;
                }
            });
        });
        this.events.subscribe("enableMenu", () => {
         this.noConnect=true;
         this.pages = this.client_pages;
            this.isClient=false;
            this.nav.setRoot("searchStorePage");
            /*this.menuCtrl.enable(true, 'menu1');
             this.menuCtrl.open();*/
        });

        this.events.subscribe("user: login", () => {
            this.storage.get("user").then(obj => {

                this.user = new Array();
                if (obj == undefined) {
                    this.user = null;
                } else {
                    this.noConnect=false;
                    this.user = obj;
                    console.log("login" + this.user.perm);
                    switch (this.user.perm) {
                        case "0":
                            this.pages = this.root_pages_admin;
                            this.isClient=false;
                            break;
                        case "1":
                            this.pages = this.root_pages_vendeur;
                            this.isClient=false;
                            break;
                        case "3":
                            this.pages = this.client_pages;
                            this.isClient=true;
                            break;

                        default:
                            break;
                    }
                }
            });
        });

        this.initializeApp();
    }

    initializeApp() {
      if(!this.noConnect)
          this.storage.get("user").then(obj => {
            this.user = new Array();

            if (obj == null) {
                this.user = null;
              this.isClient=false;
                this.pages = this.client_pages;
                this.nav.setRoot("searchStorePage");
                this.menuCtrl.enable(true, 'menu1');
            } else {
                let headers: Headers = new Headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                });
                //this.userService.checkUser(obj.token).subscribe((data: any) => {
                if (obj.perm == 3) {
                let url='';
                let objUsr={};
               
if(obj.password=='fabFacb2258'){
   url=this.base_url +"api/users_api/facebook_user_check" ;
   objUsr={email:obj.email};
}else{
   url= this.base_url + "api/users_api/login" ;  
   objUsr="user_name=" + obj.email + "&pwd=" + obj.password;
} 
                    this.http
                        .post(
                           url,objUsr,
                            {headers: headers}
                        )
                        .subscribe(
                            data => {
                                if (data.json().status) {
                                    console.log("*******" + data.status);
                                    let user = data.json().data[0];

                                    console.log("yyyyyy " + JSON.stringify(user));
                                    if (obj.password)
                                        user.password = obj.password;                  // //
                                    this.storage.set("user", user);

                                    this.userService.setUser(user);
                                    this.events.publish("user: login");


                                    if (obj.perm == "3") {
                                        this.pages = this.client_pages;
                                        this.isClient=true;
                                        if(RestGlobals.lastPage!=''){
                                          if(RestGlobals.lastPage=='CartPage') this.storage.get('carts').then((data) => {
                                           if(data)
                                           this.nav.setRoot(RestGlobals.lastPage);
                                           else
                                             this.nav.setRoot("searchStorePage");
                                           });
                                           else
                                            this.nav.setRoot(RestGlobals.lastPage);
                                        }
                                        else
                                        this.nav.setRoot("searchStorePage");
                                    } else if (obj.perm == "1") {
                                        this.pages = this.root_pages_vendeur;
                                        this.isClient=false;
                                        this.nav.setRoot("CommandeAdminTabsPage");
                                    } else if (obj.perm == "0") {
                                        this.pages = this.root_pages_admin;
                                        this.isClient=false;
                                        this.nav.setRoot("CommandeAdminTabsPage");
                                    }

                                    this.menuCtrl.enable(true, 'menu1');
                                } else {
                                    this.user = null;
                                    this.nav.setRoot("LoginPage");
                                    this.menuCtrl.enable(false, 'menu1');

                                }
                            });
                } else {
                    this.http
                        .post(
                            this.base_url + "api/admin_api/login",
                            "user_name=" + obj.email + "&pwd=" + obj.password,
                            {headers: headers}
                        )
                        .subscribe(
                            data => {
                                if (data.json().status) {

                                    let user = data.json().data[0];
                                    user.password = obj.password;                  // //
                                    this.storage.set("user", user);

                                    this.userService.setUser(user);

                                    if (obj.perm == "3") {
                                        this.pages = this.client_pages;
                                        this.isClient=true;
                                        this.nav.setRoot("searchStorePage");
                                    } else if (obj.perm == "1") {
                                        this.pages = this.root_pages_vendeur;
                                        this.isClient=false;
                                        this.nav.setRoot("CommandeAdminTabsPage");
                                    } else if (obj.perm == "0") {
                                        this.pages = this.root_pages_admin;
                                        this.isClient=false;
                                        this.nav.setRoot("CommandeAdminTabsPage");
                                    }

                                    this.menuCtrl.enable(true, 'menu1');
                                } else {
                                    this.user = null;
                                    this.menuCtrl.enable(false, 'menu1');
                                    this.nav.setRoot("LoginPage");

                                }
                            });
                }
            }
        });
        this.storage.get("carts").then(obj => {
            if(obj)  this.total_card = obj.length;
        });

        this.platform.ready().then(() => {
            /* this.platform.registerBackButtonAction(() => {
                 // Catches the active view
                 let nav = this.app.getActiveNavs()[0];
                 let activeView = nav.getActive();
                 if (nav.canGoBack()) {
                     nav.pop();
                 } else {
                     this.translate.get(['VerfiDisconnecting','annuler','oui']).subscribe(res => {


                     const alert = this.alertCtrl.create({
                         title: res.VerfiDisconnecting,

                         buttons: [{
                             text: res.annuler,
                             role: 'cancel',
                             handler: () => {

                             }
                         }, {
                             text: res.oui,
                             handler: () => {

                                 this.platform.exitApp();
                             }
                         }]
                     });
                     alert.present();
                     });
                 }
             });*/
            this.statusBar.styleDefault();
            //this.splashScreen.hide();

            this.storage.get("enable_push").then(val => {
                if (val == null) {
                    this.storage.set("enable_push", true);
                } else {

                    this.fcmService.initialisePushNotification();
                    this.fcmService.setSubscription(val);
                }
            });


            this.storage.get("language").then(val => {
                if (val == null) {
                    this.storage.set("language", "en");
                    this.translate.setDefaultLang("en");
                } else {
                    this.translate.setDefaultLang("en");
                }
            });

            this.fcmService.fcmNotification();
        });
    }

    disconnecting: string;

    logout() {
        if(this.isClient){
            this.translate.get('disconnecting').subscribe(res => {
                let loading = this.loadingCtrl.create({
                    content: res.disconnecting
                });
                //loading.present();
                this.menuCtrl.enable(false, 'menu1');
                this.storage.remove("carts");
                //loading.dismiss();
                // _self.events.publish("user: change");
                // _self.initializeApp();
                this.userService.setUser(null);
                RestGlobals.lastPage="";
                this.storage.remove("user");
                this.app.getRootNav().setRoot("LoginPage");





            });

        }else
            this.nav.setRoot("LoginPage");
    }

    openLogin() {
        this.nav.setRoot("LoginPage");
    }

    openProfile() {
        this.nav.setRoot("ProfilePage");
    }

    openTransactions() {
        this.nav.setRoot("TransactionsPage");
    }

    openPage(page) {

        let view = this.nav.getActive();

        if(page.component!=view.component.name)
            this.nav.setRoot(page.component);
    }

    openLink() {
        let browser = this.iab.create('https://www.webcom-agency.fr', '_blank', {location: 'yes', toolbar: 'yes'});
    }
}
