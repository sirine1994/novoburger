import {Http, Response, Headers} from '@angular/http';
import { Injectable } from '@angular/core';
//import { userInfo } from 'os';
import {domainConfig} from '../../config/constants';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {RestGlobals} from "../rest-globals";


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  user:any;
  constructor(public http: Http) {
    // //
  }


  getUser(){
    return this.user;
  }

  checkUser(token){
    return this.http.get(domainConfig.base_url + domainConfig.commandes_client+token)
  }
    findFreeProduct(id){
        var authHeader = new Headers();
        authHeader.append( "Content-Type", "application/x-www-form-urlencoded");
        return this.http.get(RestGlobals.apisUrl+"settings_api/product_offert?store_id="+id, {headers: authHeader}).map(RestGlobals.extractData)
            .catch(RestGlobals.handleError);
    }
  setUser(user:any){
    this.user=user;
  }

  setCard(RestGlobals_token: any,token: any): Observable<Response> {
return;
  }

  getInfoStripeCard(RestGlobals_token: any): Observable<Response> {
      var authHeader = new Headers();
      authHeader.append( "Content-Type", "application/x-www-form-urlencoded");

      return this.http.get(RestGlobals.apisUrl + "setup_api/card_detail_show?token="+RestGlobals_token, {headers: authHeader}).map(RestGlobals.extractData)
          .catch(RestGlobals.handleError);
  }
    getStripeSetup(RestGlobals_token: any): Observable<Response> {
        var authHeader = new Headers();
        authHeader.append( "Content-Type", "application/x-www-form-urlencoded");
console.log(RestGlobals.apisUrl + "setup_api/setup_create?token="+RestGlobals_token);
        return this.http.get(RestGlobals.apisUrl + "setup_api/setup_create?token="+RestGlobals_token, {headers: authHeader}).map(RestGlobals.extractData)
            .catch(RestGlobals.handleError);
    }

    

}
