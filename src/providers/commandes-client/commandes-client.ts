import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { domainConfig } from '../../config/constants';

/*
  Generated class for the CommandesClientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommandesClientProvider {
  
  constructor(public http: HttpClient) {
    // //
  }

  getCommandesClient(token: string){
    // //
    return this.http.get(domainConfig.base_url+domainConfig.commandes_client+token);
  }
  refuseCommande(token: string, id:number){ 
  
    return this.http.get(domainConfig.base_url+'api/orders_api/refuser_order?token='+token+'&id='+id);
  }
  getCommandesBy5(token: string,first:number){
    return   this.http.get(domainConfig.base_url+domainConfig.commandes_client+token+'&first='+first);
  }
 getCommandesInfo(id:number){
    return   this.http.get(domainConfig.base_url+'api/orders_api/orders?id='+id);
  }
  // http://tiliem/api/horaire_api/check?token=nPpVa4vW-Z4aL6ZAE&week=4

  IsCommandTimeValid(token: string,date:number){
   let NumberWeek = 14;
   return this.http.get(domainConfig.base_url+domainConfig.check_horaire+token+'&week='+NumberWeek)
  }
}
