import { HttpClient/*, HttpHeaders, HttpParams */} from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { RequestOptions } from '@angular/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import {domainConfig} from '../../config/constants';

/*
  Generated class for the BoCommandesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BoCommandesProvider {
  headers = new Headers();
  options: RequestOptions;  
  
  constructor(public http: HttpClient, public htt_p: Http) {
    //
    this.headers.append('Accept','application/x-www-form-urlencoded');
    this.headers.append('content-type','application/x-www-form-urlencoded');
    this.options = new RequestOptions({ headers:this.headers});
  }


  getA_EmporteCommandes(token:string, date:string,first:number){
    return this.http.get(domainConfig.base_url+'api/orders_api/order_bo_type?token='+token+'&date='+date+'&type=emporter&first='+first);
  }

  getEn_LivraisonCommandes(token: string, date:string,first:number){
    return this.http.get(domainConfig.base_url+'api/orders_api/order_bo_type?token='+token+'&date='+date+'&type=livraison&first='+first);
  }


  acceptCommande(token: string, id:number){ 
    return this.http.get(domainConfig.base_url+'api/orders_api/accept_order?token='+token+'&id='+id);
  }
    validateCommande(token: string, id:number, heure:number){
        return this.http.get(domainConfig.base_url+'api/orders_api/validate_order?token='+token+'&id='+id+'&quand='+heure);
    }

  dateModifCommande(token: string, id:number,date:string){
    let obj={token:token,id:id,date:date};
      this.headers.append('Accept','application/x-www-form-urlencoded');
      this.headers.append('content-type','application/x-www-form-urlencoded');
      let headers: Headers = new Headers({
          "Content-Type": "application/x-www-form-urlencoded"
      });
      return this.htt_p.post(domainConfig.base_url+'api/orders_api/order_quand',obj,{headers: headers});

  }


  refuseCommande(token: string, id:number){ 
  
    return this.http.get(domainConfig.base_url+'api/orders_api/refuser_order?token='+token+'&id='+id);
  }
  getHoraireBo(token:string, week:number){
    return this.http.get(domainConfig.base_url+'api/horaire_api/show?token='+token+'&week='+week);
  }

  getHistoriqueBo(token:string, date:string,first:number){
    return this.http.get(domainConfig.base_url+'api/orders_api/order_bo_historique?token='+token+'&date='+date+'&first='+first)
  }

  getCodePromos(token:string,first:number){
    return this.http.get(domainConfig.base_url+'api/code_api/show?token='+token+'&first='+first);
  }
  
  updateChalendise(token:string,chalendise:any){
    //
    let data = 'etat='+chalendise.etat
              +'&hasMontant='+chalendise.hasMontant
              +'&montant='+chalendise.montant
              +'&token='+token;

    
    return this.htt_p.post(domainConfig.base_url+'api/settings_api/update_montant',data, this.options);
  }

  updateZone(token:string, zone:any){
    let data ='paiement='+zone.paiement
             +'&type='+zone.type
             +'&zone='+zone.zone
             +'&hasZone='+zone.hasZone
             +'&token='+token;

             
    return this.htt_p.post(domainConfig.base_url+'api/settings_api/update_zone_chalandise?token='+token,data, this.options);
  }

  updateMontant(token:string, montant:any){
    let data = 'etat='+montant.etat
              +'&hasMontant='+montant.hasMontant
              +'&montant='+montant.montant
              +'$token='+token;
              
    return this.htt_p.post(domainConfig.base_url+'api/setting_api/update_montant?token='+token,data,this.options);
  }

  updateWeek(token:string, day:any){
    let dat_a='id='+day.id
             +'&dayName='+day.dayName
             +'&day='+ day.day
             +'&service='+ day.service
             +'&morning_from='+  day.morning_from
             +'&morning_to=' + day.morning_to
             +'&afternoon_from=' + day.afternoon_from
             +'&afternoon_to=' + day.afternoon_to
             +'&week='+day.week
             +'&ouverture=' + day.ouverture
             +'&token='+token;

             //
             //
    return this.htt_p.post( domainConfig.base_url+'api/horaire_api/update', dat_a, this.options);
     
  }

  add_codePromo(token:string, codePromo:any){
    let data='id='+ codePromo.id
            +'&date_from='+ codePromo.date_from
            +'&date_to='+ codePromo.date_to
            +'&code='+ codePromo.code
            +'&montant='+ codePromo.montant
            +'&reduction='+ codePromo.reduction
            +'&token='+token;
            
    return this.htt_p.post(domainConfig.base_url+'api/code_api/add', data,this.options);
  }

  getDistance(lat:number, lng: number){
    let data = 'lat='+lat 
              +'&lng='+lng;
    return this.htt_p.post(domainConfig.base_url+'api/distance_api/distance',data, this.options);
  }
}
