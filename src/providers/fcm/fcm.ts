import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// import { Push, PushObject, PushOptions } from '@ionic-native/push';
import {firebase_config} from '../../config/constants';
import {AlertController, NavController, ToastController} from 'ionic-angular';
import {Subscriber, Observable} from 'rxjs';
import {Http, Headers} from '@angular/http';
import {UserProvider} from '../user/user';
import  * as Constant from '../../config/constants';
// import { AngularFireFunctions } from '@angular/fire/functions';
// import { AngularFireMessaging } from '@angular/fire/messaging';
// import {User} from 'firebase';
import { tap } from 'rxjs/operators';
import { FCM } from '@ionic-native/fcm';
import {Storage} from "@ionic/storage";

// import {AngularFirestore} from 'angularfire2/firestore';
/*
  Generated class for the FcmProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FcmProvider {

  private _firebaseMessagingSenderId: string;
  private _firebaseServerKey: string;
  private _firebaseRegistrationId: string;
  constructor(
    private _http: HttpClient,
    public alertCtrl: AlertController,
    public storage: Storage,
    public http: Http,
    public userService: UserProvider,
    private toastController: ToastController,
    public fcm: FCM
  ) {
    this._firebaseMessagingSenderId = firebase_config.messagingSenderId;
    this._firebaseServerKey         = firebase_config.serverKey;
  }
  saveTokenToServer(token){
    let headers:Headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    let actualUser = this.userService.getUser();
    
    
    this.http.post(Constant.domainConfig.base_url+'api/users_api/fcm_set',
                      'token='+actualUser.token+
                      '&fcm='+token,
                      {headers:headers})
        .subscribe(data=>{
        },error=>console.log(error));
  }
  public subscribeForNotifications(): any {
  }


  async makeToast(message) {
  }
  initialisePushNotification(){
let that=this;
let perm;
      this.storage.get("user").then(obj => {
        if(obj) perm=obj.perm;
      });
   this.fcm.onNotification().subscribe(
      (notification: any) => {

if(perm!==2){
          if (notification.wasTapped) {

              let actionSheet = that.alertCtrl.create({
                  title: notification.title,
                  message: notification.body,
                  buttons: [
                      {
                          text: 'Ok',
                          handler: () => {
                              //that.navCtrl.setRoot("CommandeAdminTabsPage");

                          }
                      }
                      , {
                          text: "Plus tard",
                          handler: () => {

                          }

                      }
                  ]
              });
              actionSheet.present();
          } else {
              let actionSheet = that.alertCtrl.create({
                  title: notification.title,
                  message: notification.body,
                  buttons: [
                      {
                          text: 'Ok',
                          handler: () => {
                              // that.navCtrl.setRoot("CommandeAdminTabsPage");

                          }
                      }
                      , {
                          text: "Plus tard",
                          handler: () => {

                          }

                      }
                  ]
              });
              actionSheet.present();
          }
      }
      },
      (error: any) => {
        console.error(error);

      }
    );
  }

  subscribeToTopic(){
    this.fcm.subscribeToTopic('marketing')
      .then(data => console.log(data));

    this.fcm.getToken().then(token => {
      console.log(token);
      this._firebaseRegistrationId=(token);
      this.fcm.subscribeToTopic('marketing')
      this.saveTokenToServer(token);
    });

    this.fcm.onTokenRefresh().subscribe(token => {
      this._firebaseRegistrationId=(token);
      this.saveTokenToServer(token);
    });
    
    
  }
  
  unseubscribeFromTopic(){
    this.fcm.unsubscribeFromTopic('marketing');
  }

  fcmNotification(){
    this.fcm.onNotification().subscribe(data => {
      if(data.wasTapped){
        let tmpAlert = this.alertCtrl.create({
          title: data.title,
          message: data.content
        });
        tmpAlert.present();
      } else {
        let tmpAlert = this.alertCtrl.create({
          title: data.title,
          message: data.content
        });
        tmpAlert.present();
      };
    });
  }

  setSubscription(val: boolean){
    if(val == true){
      this.subscribeToTopic();
    }else{
      this.unseubscribeFromTopic();
    }
  }
}
