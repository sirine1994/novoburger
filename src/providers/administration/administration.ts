import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { domainConfig } from '../../config/constants';
/*
  Generated class for the AdministrationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AdministrationProvider {

  constructor(public http: HttpClient) {
    // //
  }

  checkOpenTime(token: string, date: string,lat,long,idStore){
    return this.http.get(domainConfig.base_url+ domainConfig.check_horaire+token+'&date='+date+'&lat='+lat+'&lng='+long+'&store_id='+idStore);
  }

  checkZoneChalendise(token:string,id){
    return this.http.get(domainConfig.base_url+domainConfig.setting_zone_chalandise+token+"&store_id="+id);
  }

  checkMontant(token:string,storeId){
    return this.http.get(domainConfig.base_url+domainConfig.setting_montant+token+"&store_id="+storeId);
  }
}
