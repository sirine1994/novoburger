import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

/*
  Generated class for Global rest vars.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

export class RestGlobals {
//"https://cors-anywhere.herokuapp.com/"+
    public static serverUrl: string = "https://extranet.novo-burger.fr/";
    public static apisUrl: string = RestGlobals.serverUrl + "api/";
    public static isClient:boolean;
public static discount:string='';
public static myadress:string='';
public static montant:string='';
    public static lastPage:string='';
    public static stripeAccount:string="";
    public static idStore:number;
    public static storeName:string='';
    public static codepormo:string='';
    public static noteCmd:string='';
     public static data:any=null;
    constructor() {
    }

    public static extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    public static handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
