import {CommandesClientProvider} from './../../providers/commandes-client/commandes-client';
import {Component} from '@angular/core';
import {
    IonicPage,
    NavController,
    ToastController,
    ModalController,
    NavParams,
    Events,
    LoadingController
} from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {AlertController} from 'ionic-angular';
import {SocialSharing} from '@ionic-native/social-sharing';
import {AboutPage} from '../about/about';
import {CallNumber} from '@ionic-native/call-number';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {Platform} from 'ionic-angular';
import {CurrencyProvider} from '../../providers/currency';
import {UserProvider} from '../../providers/user/user';
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the CommandesClientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-commandes-client',
    templateUrl: 'commandes-client.html',
})
export class CommandesClientPage {


    base_url: any;
    total_card: number=0;

    carts: Array<any>;
    favoriest_list: Array<any>;

    list: Array<any>;
    first: number;
    settings: any = '';

    commandesClient: any[];
    soldeCaisse: any;
    user: any;
    loading: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public callNumber: CallNumber,
                public iab: InAppBrowser,
                public http: Http,
                public storage: Storage,
                public translate: TranslateService,
                private alertCtrl: AlertController,
                public toastCtrl: ToastController,
                public socialSharing: SocialSharing,
                public platform: Platform,
                public currencyProvider: CurrencyProvider,
                public modalCtrl: ModalController,
                private events: Events,
                public commandeClientsService: CommandesClientProvider,
                public loadingCtrl: LoadingController,
                public userService: UserProvider) {
        this.commandesClient = [];
        this.currencyProvider;

        this.base_url = Constant.domainConfig.base_url;
        this.carts = Array();
        this.favoriest_list = Array();
        this.storage.ready().then(() => {
            this.storage.get('settings').then(data => {
                this.settings = data;
            })

            this.storage.get('carts').then((obj) => {
                if (obj == null) {
                    this.storage.set('carts', this.carts);
                }
            });

            this.storage.get('favoriest_list').then((data) => {
                if (data == null) {
                    this.storage.set('favoriest_list', this.favoriest_list);
                    this.favoriest_list = new Array();
                } else {
                    this.favoriest_list = data;
                }

                this.first = 0;
                this.list = new Array();
                this.loadMore();
            });
        });

        events.subscribe('user:add_cart', (user, time) => {
            this.total_card += 1;
        });
    }


    ionViewDidLoad() {
        // // //
       
    }

    ionViewWillEnter() {

        if (this.userService.getUser() !== undefined) {
            this.commandeClientsService.getCommandesBy5(this.userService.getUser().token, 1)
                .subscribe((orderClient: any) => {
                    // //
                    this.soldeCaisse = orderClient.soldeCaisse;
                    this.commandesClient = orderClient.data;
                   // console.log(this.commandesClient);
                });
        }
        this.storage.get('carts').then((obj) => {
            this.total_card = obj.length;
        });
    }

    cart(){
        this.navCtrl.push('CartPage')
    }

    opendDetails(item) {

        this.navCtrl.push("DetailCommandePage", {
            command: item
        });
    }

    loadMore(infiniteScroll: any = null) {
        this.first += 1;


        this.commandeClientsService.getCommandesBy5(this.userService.getUser().token, this.first)
            .subscribe((orderCommand: any) => {

                    orderCommand.data.forEach(x => {
                        this.commandesClient.push(x);
                    });
                    if (infiniteScroll) {
                        infiniteScroll.complete();
                    }
                },
                error => {
                    if (infiniteScroll != null) {
                        infiniteScroll.enable(false);
                    }
                });

    }

    addFavoriest(item) {
        if (item.favoriest) {
            item.favoriest = false;
            let index_of = this.favoriest_list.indexOf(item.id);
            this.favoriest_list.splice(index_of, 1);
        } else {
            item.favoriest = true;
            this.favoriest_list.push(item.id);
        }
        this.storage.set('favoriest_list', this.favoriest_list);
    }

    send_message(id) {
        this.navCtrl.push(AboutPage,{idcmd:id});

    }


    modalAddCart(item) {
        // //
        let modal = this.modalCtrl.create('AddCartPage', {
            'food_id': item.id,
            'discount': item.discount,
            'price': item.price
        });
        modal.present();
    }

    openPage(id) {
        this.navCtrl.setRoot('FoodDetailPage', {id: id});
    }

    openCartPage() {
        this.translate.get(['loading_data']).subscribe(res => {

            this.loading = this.loadingCtrl.create({
                content: res.loading_data
            });
        });
            this.loading.present();
            this.navCtrl.setRoot('CartPage', {loading: this.loading});


    }

    annulerC(id) {
        this.translate.get(['cancel_order', 'oui', 'annuler']).subscribe(res => {
            let headers: Headers = new Headers({
                "Content-Type": "application/x-www-form-urlencoded"
            });
            let that = this;
            let alert = this.alertCtrl.create({
                message: res.cancel_order,
                buttons: [
                    {
                        text: res.oui,
                        handler: data => {
                            //this.navCtrl.pop();
                            that.http.get(Constant.domainConfig.base_url + "api/orders_api/annulation_client_order?token=" + that.userService.getUser().token + "&id=" + id, {
                                headers: headers
                            }).subscribe(
                                (data: any) => {
                                    that.wsSuccess(data.json());
                                },
                                error => {

                                }
                            );
                        }
                    },
                    {
                        text: res.annuler,
                        role: "cancel",
                        handler: data => {

                        }
                    }
                ]
            });
            alert.present();
        });

    }

    wsSuccess(data) {
        this.commandeClientsService.getCommandesBy5(this.userService.getUser().token, 1)
            .subscribe((orderClient: any) => {
                // //
                this.commandesClient = orderClient.data;
                console.log(this.commandesClient);
            });
    }

    openCatPage() {
        this.translate.get(['loading_data']).subscribe(res => {

            this.loading = this.loadingCtrl.create({
                content: res.loading_data
            });
        });
        this.loading.present();
        this.navCtrl.setRoot('CategoriesPage', {loading: this.loading});
    }

    openSearchPage() {

        this.translate.get(['loading_data']).subscribe(res => {

            this.loading = this.loadingCtrl.create({
                content: res.loading_data
            });
        });
        this.loading.present();
        this.navCtrl.setRoot('SearchPage', {loading: this.loading});
    }

    share(item) {
        this.socialSharing.share(item.name, item.description, null, Constant.domainConfig.base_url + 'food?id=' + item.id);
    }

    facebook() {
        let browser = this.iab.create(this.settings.facebook, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    twitter() {
        let browser = this.iab.create(this.settings.twitter, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    call() {
        this.callNumber.callNumber(this.settings.phone, true);
    }

}
