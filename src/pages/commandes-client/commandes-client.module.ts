import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommandesClientPage } from './commandes-client';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CommandesClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CommandesClientPage),
    TranslateModule.forChild()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CommandesClientPageModule {}
