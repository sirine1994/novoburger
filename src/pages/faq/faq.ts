import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RestGlobals} from "../../providers/rest-globals";
import {Http} from "@angular/http";

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
    showDiv:any;
    faq:any;
  constructor(public navCtrl: NavController,public http: Http, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
      this.http.get(RestGlobals.apisUrl + "faq_api/list").subscribe(data=>{

           if(data.json().aide!=null){
                      this.faq = data.json().aide;
                      this.showDiv= new Array();
                      for(let j;j<this.faq.length;j++)
                          this.showDiv.push(false);
                  }else{
                      this.faq = null;

                  }
      })
  }
    btnhide(i) {

        this.showDiv[i] = !(this.showDiv[i]);

        for(let j =0;j<this.showDiv.length;j++)
            if(j!=i)
                this.showDiv[j]=false;
    }

    cart(){
        this.navCtrl.push('CartPage')
    }
}
