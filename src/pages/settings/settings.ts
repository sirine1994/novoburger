import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  LoadingController
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import * as Constant from "../../config/constants";
import { Storage } from "@ionic/storage";
import { AdministrationProvider } from "../../providers/administration/administration";
import { UserProvider } from "../../providers/user/user";
// import { OneSignal } from "@ionic-native/onesignal";
import { BoCommandesProvider } from "../../providers/bo-commandes/bo-commandes";
import {FcmProvider} from "../../providers/fcm/fcm";
import {RestGlobals} from "../../providers/rest-globals";
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  toggle: any;
  radioOpen: boolean;
  radioResult;
  token: string;
  selectedLang: any = null;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public storage: Storage,
    public modalCtrl: ModalController,
    public administrationService: AdministrationProvider,
    public userService: UserProvider,
    public fcmService: FcmProvider,
    public boCommandeService: BoCommandesProvider,
    public loadingCtrl: LoadingController
  ) {
    this.storage.get("language").then(lang => {
      this.selectedLang = lang;
    });
    this.storage.get("enable_push").then(val => {
      this.toggle = val;
    });
  }
  chalendise: any;
  zone: any;
  ionViewDidLoad() {
      this.translate.get(['loadingC']).subscribe(res => {
          let loader = this.loadingCtrl.create({
      content: res.loadingC
    });
   // loader.present();

    this.token = this.userService.getUser().token;

    this.administrationService
      .checkZoneChalendise(this.token,RestGlobals.idStore)
      .subscribe((data: any) => {
        //
        this.zone = data.data.setting_zone;

         loader.dismiss();
        /*this.administrationService
          .checkMontant(this.token)
          .subscribe((data: any) => {
            //
            this.chalendise = data.data.setting_montant;

            loader.dismiss();
          });*/
      });
      });
  }

  push_setting() {
    let modal = this.modalCtrl.create("PushSettingPage");
    modal.present();
  }
  togglePush() {
    this.storage.get("enable_push").then(val => {
      if (val == false) {
        this.storage.set("enable_push", true);
        this.fcmService.setSubscription(true);
      } else {
        this.storage.set("enable_push", false);
        this.fcmService.setSubscription(false);
      }
    });
  }
  language() {
      this.translate.get(['languages','annuler','ok']).subscribe(res => {

          let boxalert = this.alertCtrl.create();
    boxalert.setTitle(res.languages);
    let langs = Constant.languages;
    for (let key in langs) {
      if (key == this.selectedLang) {
        boxalert.addInput({
          type: "radio",
          label: langs[key],
          value: key,
          checked: true
        });
      } else {
        boxalert.addInput({
          type: "radio",
          label: langs[key],
          value: key
        });
      }
    }

    boxalert.addButton(res.annuler);
    boxalert.addButton({
      text: res.ok,
      handler: data => {
        this.radioOpen = false;
        this.radioResult = data;
        this.storage.set("language", "en");
        this.translate.setDefaultLang("en");
        this.selectedLang = data;
      }
    });
    boxalert.present();
      });
  }
  updateChalendise() {
    //
      this.translate.get(['update_in_prog','server_error']).subscribe(res => {
          let loader = this.loadingCtrl.create({
              content: res.update_in_prog
          });
          loader.present();
          this.boCommandeService
              .updateChalendise(this.token, this.chalendise)
              .subscribe(
                  data => {
                      //
                      loader.dismiss();
                  },
                  err => {
                      loader.dismiss();
                      let alert = this.alertCtrl.create({
                          message: res.server_error
                      });
                      alert.present();
                  }
              );
      });
  }
  updateZone() {
      this.translate.get(['update_in_prog','server_error']).subscribe(res => {
          let loader = this.loadingCtrl.create({
              content: res.update_in_prog
          });
          loader.present();
          this.boCommandeService.updateZone(this.token, this.zone).subscribe(
              data => {
                  //
                  loader.dismiss();
              },
              err => {
                  loader.dismiss();
                  let alert = this.alertCtrl.create({
                      message: res.server_error
                  });
                  alert.present();
              }
          );
      });
  }
  toggleSettingZone(event) {
	//
	//
          this.zone.hasZone = event._value ;
  if(event._value == true){
    this.zone.paiement = 'All';
    this.zone.type = 'All';
    this.zone.zone = 5 ;
  }else{
    this.zone.paiement = 'On_line';
    this.zone.type = 'Emporter';
  }

	this.updateZone();
  }

  updateChalendisePaiement(event) {
	//
	this.zone.paiement = event;
	this.updateZone();
  }

  updateChalendiseType(event) {
    //
	this.zone.type = event;
	this.updateZone();
  }

  updateChalendiseDistance(event) {
    //
	this.zone.zone =event._value;
	this.updateZone();
  }

  toggleSettingsMontant(event) {
	//
  this.chalendise.hasMontant = event._value;

  if(event._value == true){
    this.chalendise.etat = 1;
  }else{
    this.chalendise.etat = 0;
  }

	this.updateChalendise();
  }

  updateMontant(event) {
	//
	this.chalendise.montant = event._value;
	this.updateChalendise();
  }
}
