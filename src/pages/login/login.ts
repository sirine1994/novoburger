import {UserProvider} from "./../../providers/user/user";
import {Component, ViewChild} from "@angular/core";
import {
    IonicPage,
    Nav,
    NavController,
    NavParams,
    AlertController,
    LoadingController, MenuController
} from "ionic-angular";
import {Http, Headers} from "@angular/http";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import * as Constant from "../../config/constants";
import {Facebook, FacebookLoginResponse} from "@ionic-native/facebook";
import {HomePage} from "../home/home";
import {MyApp} from "../../app/app.component";
import {RestGlobals} from "../../providers/rest-globals"
import {CategoriesPage} from "../categories/categories";
import {TranslateService} from "@ngx-translate/core";
import {searchStorePage} from "../searchStore/searchStore";
import { GooglePlus } from '@ionic-native/google-plus';
declare var cordova: any;
@IonicPage()
@Component({
    selector: "page-login",
    templateUrl: "login.html"
})
export class LoginPage {
    @ViewChild(Nav) nav: Nav;
    isAdmin = false;
    isClient = true;
    rootPage: any = CategoriesPage;

    roleString: string;

    base_url: any = "";
    user_name: any = "";
    password: any = "";
    msg_err: any = null;

    constructor(public navCtrl: NavController,
    private googlePlus: GooglePlus,
                public navParams: NavParams,
                public events: Events,
                public http: Http,
                public storage: Storage,
                public fb: Facebook,
                public translate: TranslateService,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                private menuCtrl:MenuController,
                public userService: UserProvider) {
        this.roleString = "Client";
        this.base_url = Constant.domainConfig.base_url;
    }

    login() {
        RestGlobals.isClient = this.isClient;
        this.translate.get(['user_verification', 'err_login', 'missing_info']).subscribe(res => {
            let loading = this.loadingCtrl.create({
                content: res.user_verification
            });


            if (this.user_name != '' &&
                this.password != ''
            ) {
                loading.present();
                let headers: Headers = new Headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                });

                switch (this.isClient) {
                    case false:
                        this.http
                            .post(
                                this.base_url + "api/admin_api/login",
                                "user_name=" + this.user_name + "&pwd=" + this.password,
                                {headers: headers}
                            )
                            .subscribe(
                                data => {
                                    loading.dismiss();

                                    if (data.json().empty == null) {
                                        let user = data.json().data[0];
                                        user.password = this.password;                  // //
                                        this.storage.set("user", user);

                                        this.userService.setUser(user);
                                        // this.events.publish('user: change');
                                        this.events.publish("user: login");

                                        this.navCtrl.setRoot(MyApp);
                                    } else {
                                        this.msg_err = res.err_login;
                                    }
                                },
                                error => {
                                    loading.dismiss();
                                }
                            );
                        //
                        break;

                    case true:
                        this.http
                            .post(
                                this.base_url + "api/users_api/login",
                                "user_name=" + this.user_name + "&pwd=" + this.password,
                                {headers: headers}
                            )
                            .subscribe(
                                data => {

                                    loading.dismiss();

                                    if (data.json().status == true) {

                                        let user = data.json().data[0];
                                        user.password = this.password;                  // //
                                        this.storage.set("user", user);

                                        this.userService.setUser(user);

                                        // this.events.publish('user: change');
                                        this.events.publish("user: login");


                                        this.navCtrl.setRoot(MyApp);
                                        // alert.present();
                                    } else {
                                        this.msg_err = res.err_login;
                                    }
                                },
                                error => {
                                }
                            );

                        break;
                    default:
                        break;
                }
            } else {
                this.msg_err = res.missing_info;

                // alert.present();
            }
        });
    }

    decouvrir(){
       if(RestGlobals.lastPage!='' && this.userService.getUser() && typeof (this.userService.getUser().token)!="undefined")
           this.navCtrl.setRoot(RestGlobals.lastPage);
           else
             this.navCtrl.setRoot("searchStorePage");
    }
    roleChanged(event: any) {
        if (this.isAdmin) {
            this.roleString = "Admin";
        } else {
            this.roleString = "Client";
        }
    }

   apple_login() {
let headers: Headers = new Headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                });
               
   cordova.plugins.SignInWithApple.signin(
{ requestedScopes: [0, 1] },
(appleLoginResponse: any) => {

 this.http
                                .post(
                                    this.base_url +
                                    "api/users_api/apple_user_check",{user:appleLoginResponse.user,identifyToken:appleLoginResponse.identityToken},{headers: headers}
                                )
                                .subscribe(
                                    data => {
                                        //check user facebook
                                      // alert(JSON.stringify(data.json()));
                                        if (!data.json().status) {
                                        this.navCtrl.push("SignupPage",{dataUser:{email:appleLoginResponse.email,name:appleLoginResponse.fullName.familyName}});

                                          /*  let headers: Headers = new Headers({
                                                "Content-Type": "application/x-www-form-urlencoded"
                                            });

                                            let username = appleLoginResponse.email.substring(
                                                0,
                                                appleLoginResponse.indexOf("@")
                                            );
                                           
                                            username = username.substring(0, 4) + "_" + appleLoginResponse.user;

                                            let params =
                                                "fb_id=" +
                                                appleLoginResponse.identityToken +
                                                "&email=" +
                                                appleLoginResponse.email +
                                                "&fullname=" +
                                                appleLoginResponse.fullName.familyName +
                                                "&user_name=" +
                                                username;
                                             
                                            this.http
                                                .post(
                                                    this.base_url + "api/users_api/facebook_user_register",
                                                    params,
                                                    {headers: headers}
                                                )
                                                .subscribe(
                                                    data => {
                                                        // //
                                                        let user = data.json();
                                                        user = user[0];
                                                      
                                                        this.storage.set("user", user);
                                                        this.events.publish("user: change");
                                                        //this.nav.setRoot(HomePage);
                                                    },
                                                    error => {
                                                        
                                                    }
                                                );*/
                                        } else {
                                       // alert("else"+data.json());
                                        let user = data.json().data[0];
                                        user.password = "fabFacb2258";                  // //
                                        this.storage.set("user", user);

                                        this.userService.setUser(user);

                                        // this.events.publish('user: change');
                                        this.events.publish("user: login");


                                        this.navCtrl.setRoot(MyApp);
                                        }
                                    },
                                    error => {
                                   // alert(JSON.stringify(error));
                                    }
                                );

},
(err: any) => {
console.error(err);
//alert("end "+JSON.stringify(err));
}
) 
}

 fb_login(){
    
                       let headers: Headers = new Headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                });
               



                  
                  this.translate.get('err_login_fb').subscribe(res => {
            this.fb
                .login(["public_profile", "email"])
                .then((res: FacebookLoginResponse) => {
                    let params = new Array<string>();
                    this.fb
                        .api("/me?fields=name,gender,email", params)
                        .then(user => {
                         
                            this.http
                                .post(
                                    this.base_url +
                                    "api/users_api/facebook_user_check",{email:user.email},{headers: headers}
                                )
                                .subscribe(
                                    data => {
                                        //check user facebook
                                        if (data.json().success == 0) {
                                        this.navCtrl.push("SignupPage",{dataUser:{email:user.email,name:user.name}});

                                           
                                        } else {
                                        let user = data.json().data[0];
                                        user.password = "fabFacb2258";                  // //
                                        this.storage.set("user", user);

                                        this.userService.setUser(user);

                                        // this.events.publish('user: change');
                                        this.events.publish("user: login");


                                        this.navCtrl.setRoot(MyApp);
                                        }
                                    },
                                    error => {
                                    }
                                );
                        })
                        .catch(e => {
                            this.translate.get(['err_login_fb']).subscribe(res => {
                                //alert(res.err_login_fb + JSON.stringify(e));
                        });
                        });
                })
                .catch(e => {

                   // alert(res.err_login_fb + JSON.stringify(e));

                });
        });
    }

     gmail_login() {
     let headers: Headers = new Headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                });
   this.googlePlus.login({})
      .then(user => {

        this.http
                                .post(
                                    this.base_url +
                                    "api/users_api/facebook_user_check",{email:user.email},{headers: headers}
                                )
                                .subscribe(
                                    data => {
                                        //check user facebook
                                        if (data.json().success == 0) {
                                        this.navCtrl.push("SignupPage",{dataUser:{email:user.email,name:user.name}});

                                           
                                        } else {
                                        let user = data.json().data[0];
                                        user.password = "fabFacb2258";                  // //
                                        this.storage.set("user", user);

                                        this.userService.setUser(user);

                                        // this.events.publish('user: change');
                                        this.events.publish("user: login");


                                        this.navCtrl.setRoot(MyApp);
                                        }
                                    },
                                    error => {
                                    }
                                );


      })
      .catch(err => {
        //  alert(JSON.stringify(err))
      });
  }

  logout() {
    this.googlePlus.logout()
      .then(res => {
        console.log(res);
       /* this.displayName = "";
        this.email = "";
        this.familyName = "";
        this.givenName = "";
        this.userId = "";
        this.imageUrl = "";

        this.isLoggedIn = false;*/
      })
      .catch(err => console.error(err));
  }

   signup() {
           
          this.navCtrl.push("SignupPage");
    }

    forgetPassword() {
        this.translate.get(['forget_password', 'annuler', 'ok']).subscribe(res => {

            let alert = this.alertCtrl.create({
                title: res.forget_password,
                inputs: [
                    {
                        name: 'email',
                        placeholder: 'Renseignez votre email'
                    }
                ],
                buttons: [
                    {
                        text: res.annuler,
                        role: 'cancel',
                        handler: data => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: res.ok,
                        handler: data => {
                            let headers: Headers = new Headers({
                                "Content-Type": "application/x-www-form-urlencoded"
                            });


                            let params =

                                "email=" +
                                data.email;
                            this.http
                                .post(
                                    this.base_url + "api/users_api/sendLink",
                                    params,
                                    {headers: headers}
                                )
                                .subscribe(
                                    data => {
                                        // //


                                        this.wsForgetSuccess(data.json());
                                    },
                                    error => {
                                        // //
                                    }
                                );
                        }
                    }
                ]
            });
            alert.present();
        });
    }

    wsForgetSuccess(data) {
        this.translate.get(['check_mailbox', 'ok']).subscribe(res => {

            let actionSheet = this.alertCtrl.create({
                title: res.check_mailbox,
                message: 'Un email vous permettant de réinitialiser votre mot de passe vient de vous être envoyé.',
                buttons: [
                    {
                        text: res.ok,

                        handler: () => {

                        }
                    }
                ], enableBackdropDismiss: false
            });

            if (data.hasPermission)
                actionSheet.present();
        });
    }
}
