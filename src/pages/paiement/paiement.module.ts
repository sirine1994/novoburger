import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaiementPage } from './paiement';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    PaiementPage,
  ],
  imports: [
    IonicPageModule.forChild(PaiementPage),
      TranslateModule.forChild()
  ],
})
export class PaiementPageModule {}
