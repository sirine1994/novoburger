import { Component } from '@angular/core';
import {AlertController, IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the CouponFidelitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-coupon-fidelite',
  templateUrl: 'coupon-fidelite.html',
})
export class CouponFidelitePage {


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CouponFidelitePage');
  }

    cart(){
        this.navCtrl.push('CartPage')
    }
}
