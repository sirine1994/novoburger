import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CouponFidelitePage } from './coupon-fidelite';

@NgModule({
  declarations: [
    CouponFidelitePage,
  ],
  imports: [
    IonicPageModule.forChild(CouponFidelitePage),
  ],
})
export class CouponFidelitePageModule {}
