import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController} from 'ionic-angular';
import {BoCommandesProvider} from '../../providers/bo-commandes/bo-commandes';
import {UserProvider} from '../../providers/user/user';
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the HoraireBoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-horaire-bo',
    templateUrl: 'horaire-bo.html',
})
export class HoraireBoPage {
    public event = {
        month: new Date().toISOString().split('T')[0],
        timeStarts: '07:43',
        timeEnds: '1990-02-20'
    }
    week: number;
    horaire: any;

    cucumber: boolean;

    toDay: Date;

    token: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public translate: TranslateService,
                public commandeBoService: BoCommandesProvider,
                public userService: UserProvider,
                public modalController: ModalController,
                public loadingCtrl: LoadingController,
                private alertCtrl: AlertController) {
    }

    updateCucumber() {
        //
    }


    ionViewDidLoad() {

        this.token = this.userService.getUser().token;
        this.toDay = new Date();

        this.week = Math.ceil((Number(new Date().toISOString().split('T')[0].split('-')[1]) * 30 + Number(new Date().toISOString().split('T')[0].split('-')[2])) / 7);
        this.getHoraire(this.week);

    }

    private getHoraire(week) {
        this.translate.get(['loading_data', 'Operation_not_performed']).subscribe(res => {

            let loader = this.loadingCtrl.create({
                content: res.loading_data
            });
            loader.present();
            this.commandeBoService.getHoraireBo(this.token, week)
                .subscribe((data: any) => {
                        this.horaire = data;
                        //

                        loader.dismiss();
                    },
                    err => {
                        loader.dismiss();
                        let alert = this.alertCtrl.create({
                            'message': res.Operation_not_performed
                        });
                        alert.present();
                    });
        });
    }

    weekChanged(event: any, week: string) {
        this.week = Number(week);
        this.getHoraire(week);
    }

    getDayService(ouverture: string): boolean {

        if (ouverture == '1') {
            return true;
        } else {
            return false;
        }
    }

    async getdayDetails(event, day: any) {
        //
        const modal = await this.modalController.create(
            'DayDetailsPage',
            {details: day}
        );
        await modal.present();

        modal.onDidDismiss((data) => {
            //
        });
    }


    updateWeek(event: any, day: any) {
        //
        if (event._value == true) {
            day.ouverture = 1;
        } else {
            day.ouverture = 0;
        }
        this.commandeBoService.updateWeek(this.token, day)
            .subscribe((data: any) => {
                    this.getHoraire(day.week);
                },
                err => {
                    //
                })
    }
}