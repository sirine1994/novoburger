import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HoraireBoPage } from './horaire-bo';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    HoraireBoPage,
  ],
  imports: [
    IonicPageModule.forChild(HoraireBoPage),
      TranslateModule.forChild()
  ],
  entryComponents:[
    HoraireBoPage
  ]
})
export class HoraireBoPageModule {}
