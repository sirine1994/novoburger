import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { BoCommandesProvider } from '../../../providers/bo-commandes/bo-commandes';
import { UserProvider } from '../../../providers/user/user';

/**
 * Generated class for the DayDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-day-details',
  templateUrl: 'day-details.html',
})
export class DayDetailsPage {
  isOnService:boolean;
  data:any;

  constructor(public view:ViewController, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              public commandeBoService: BoCommandesProvider,
              public userService: UserProvider,
              public loadingCtrl:LoadingController,
              private alertCtrl: AlertController) {
  }
  ionViewWillLoad() {
    this.data=this.navParams.get('details');
    //
    if(this.data.service == '1'){
      this.isOnService = true;
    }else if (this.data.service == '0'){
      this.isOnService = false;
    }
  }
  ionViewDidLoad() {
    //
  }
  closePage(){
    const data = {
      name: 'Ahmed Schhaider',
      occupation: 'Developer'
    }

    this.view.dismiss(data);
  }

  // ionPickerDidDismiss(){
  //   this.updateDay(false);
  // }
  updateDayService(event){
    //
    if(event._value == true) {
      this.data.service = 1;
    } else {
      this.data.service = 0;
    }

    this.updateDay();
  }
  updateDay(){
    let loader = this.loadingCtrl.create({
      content: 'Chargement de données'
    });
    loader.present();
    this.commandeBoService.updateWeek(this.userService.getUser().token, this.data)
      .subscribe(data=>{
        //
        loader.dismiss();
      },
			err=>{
				loader.dismiss();
				let alert=this.alertCtrl.create({
					'message':'Operation non effectué'
				  });
				  alert.present();
			});
  }
}
