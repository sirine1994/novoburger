import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DayDetailsPage } from './day-details';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    DayDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DayDetailsPage),
      TranslateModule.forChild()
  ],
  exports:[
    DayDetailsPage
  ]
})
export class DayDetailsPageModule {}
