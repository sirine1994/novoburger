import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
//import {EditAddressPage} from "../edit-address/edit-address";
import {RestGlobals} from "../../providers/rest-globals";
import * as Constant from "../../config/constants";
import {Http} from "@angular/http";
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the AddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})

export class AddressPage {
    list: any;
  constructor(public navCtrl: NavController,
              public http: Http,
              public navParams: NavParams,
              public userService: UserProvider,
              public modalCtrl:ModalController) {
  }

  ionViewDidLoad() {
    this.loadMore();
  }

loadMore(){
    this.http.get(Constant.domainConfig.base_url+'api/adresse_api/get?token='+this.userService.getUser().token ).subscribe(data=>{
        this.list = data.json().mes_adresses;


    });
}
    deleteAdrr(id){
        this.http.get(Constant.domainConfig.base_url+'api/adresse_api/remove?token='+this.userService.getUser().token+"&id="+id ).subscribe(data=>{

this.loadMore();

        });

    }

    cart(){
        this.navCtrl.push('CartPage')
    }
    addAddress(list){
            this.navCtrl.push("EditAddressPage",{list:list});


    }
}
