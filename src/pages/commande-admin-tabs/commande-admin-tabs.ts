import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {FcmProvider} from "../../providers/fcm/fcm";

/**
 * Generated class for the CommandeAdminTabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commande-admin-tabs',
  templateUrl: 'commande-admin-tabs.html'
})
export class CommandeAdminTabsPage {

  aEmporterRoot = 'AEmporterPage';
  enLivraisonRoot = 'EnLivraisonPage';


  constructor(public navCtrl: NavController,public myfcm:FcmProvider) {

  }

    ionViewDidLoad(){

    this.myfcm.subscribeToTopic();
    this.myfcm.initialisePushNotification();
  }
}
