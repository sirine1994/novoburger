import {Component} from '@angular/core'
import {
    IonicPage, NavController, NavParams, Events, LoadingController, AlertController,
    ModalController
} from 'ionic-angular'
import {BoCommandesProvider} from '../../../providers/bo-commandes/bo-commandes'
import {UserProvider} from '../../../providers/user/user';
import {TranslateService} from "@ngx-translate/core";
// import { DatePipe } from '@angular/common'
// import { Storage } from '@ionic/storage';
/**
 * Generated class for the AEmporterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-a-emporter',
    templateUrl: 'a-emporter.html',
})
export class AEmporterPage {
    public event = {
        month: new Date().toISOString().split('T')[0],
        timeStarts: '07:43',
        timeEnds: '1990-02-20'
    };
    first = 1;
    commandesList: any[];

    token: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public modalCtrl: ModalController,
                public translate: TranslateService,
                public commandeBoService: BoCommandesProvider,
                public userService: UserProvider,
                public loadingCtrl: LoadingController,
                private alertCtrl: AlertController) {

    }

    ionViewDidEnter() {
        this.loadCommands();
    }

    ionViewDidLoad() {
        //
        this.token = this.userService.getUser().token;
        this.loadCommands();
    }

    private loadCommands() {
        this.translate.get(['loading_data', 'Operation_not_performed']).subscribe(res => {

            let loader = this.loadingCtrl.create({
                content: res.loading_data
            });
            loader.present();
            this.commandeBoService.getA_EmporteCommandes(this.token, this.event.month, this.first)
                .subscribe((data: any) => {
                        //
                        this.commandesList = data.data;

                        loader.dismiss();
                    },
                    err => {
                        loader.dismiss();
                        let alert = this.alertCtrl.create({
                            'message': res.Operation_not_performed
                        });
                        alert.present();
                    });
        });
    }

    opendDetails(item) {

        let modal = this.modalCtrl.create("DetailCommandePage", {
            command: item
        });
        modal.present();
    }

    loadMore(infiniteScroll: any = null) {


        this.first += 1;


        this.commandeBoService.getA_EmporteCommandes(this.token, this.event.month, this.first)
            .subscribe((orderCommand: any) => {
                    orderCommand.data.forEach(x => {
                        this.commandesList.push(x);
                    });
                    if (infiniteScroll) {
                        infiniteScroll.complete();
                    }

                },
                error => {
                    if (infiniteScroll != null) {
                        infiniteScroll.enable(false);
                    }
                });

    }

    getDayService(validation: string): boolean {

        if (validation == '1') {
            return true;
        } else {
            return false;
        }
    }


    dateChanged(event) {
        this.loadCommands();
    }

    refuseCommand(command: any) {
        this.translate.get(['cancel_in_prog']).subscribe(res => {
            let loader = this.loadingCtrl.create({
                content: res.cancel_in_prog
            });
            loader.present();

            this.commandeBoService.refuseCommande(this.token, command.id)
                .subscribe((data: any) => {
                    this.loadCommands();
                    loader.dismiss();
                });
        });
    }

    acceptCommand(command: any) {
        this.translate.get(['accept_in_prog']).subscribe(res => {
            let loader = this.loadingCtrl.create({
                content: res.accept_in_prog
            });
            loader.present();
            this.commandeBoService.acceptCommande(this.token, command.id)
                .subscribe((data: any) => {

                    this.loadCommands();
                    loader.dismiss()
                });
        });
    }

    modifyDateCommand(command: any) {
        let today = new Date();
        let mdd: any = today.getDate();
        let mm: any = today.getMonth() + 1;
        const yyyy = today.getFullYear();
        if (mdd < 10) {
            mdd = mdd;
        }
        if (mm < 10) {
            mm = `0${mm}`;
        }
        let tod = mdd + "/" + mm + "/" + yyyy;
        this.translate.get(['date_liv', 'date_resv', 'annuler', 'send']).subscribe(res => {
            let alertt = this.alertCtrl.create({
                title: res.date_liv,
                inputs: [
                    {
                        label: 'Date',
                        name: 'Date',
                        type: 'date',
                        value: new Date().toISOString(),
                        min: new Date().toISOString(),
                        placeholder: res.date_resv
                    }
                ],
                buttons: [
                    {
                        text: res.annuler,
                        role: 'cancel',
                        handler: data => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: res.send,
                        handler: data => {

                            this.commandeBoService.dateModifCommande(this.token, command.id, data.Date)
                                .subscribe((data: any) => {
                                    this.loadCommands();

                                });
                        }
                    }
                ]
            });
            alertt.present();
        });

    }

    /*    showAlertInfo() {

            let alert = this.alertCtrl.create({
                title: '',
                subTitle: 'Facture commande',
                inputs: [
                    {
                        name: 'caisse',
                        type: 'number',
                        placeholder: 'Montant en caissé'
                    },
                    {
                        name: 'cb',
                        type: 'number',
                        placeholder: 'CB'
                    },
                    {
                        name: 'espace',
                        type: 'number',
                        placeholder: 'Espace'
                    },
                    {
                        name: 'ticket',
                        type: 'number',
                        placeholder: 'Ticket'
                    }
                ],
                buttons: [{
                    text: 'commande traitée', handler: () => {
                        //   this.goToTutorial();
                    }
                }]

            });

            alert.present();
        }*/

    validateCommand(command: any) {
        this.translate.get(['valid_in_prog']).subscribe(res => {


            let alert = this.alertCtrl.create({
                title: '',
                subTitle: 'Heure souhaité',
                inputs: [
                    {
                        name: 'Heure',
                        type: 'time',

                    }
                ],
                buttons: [{
                    text: 'commande traitée',handler: data => {
                        let loader = this.loadingCtrl.create({
                            content: res.valid_in_prog
                        });
                        loader.present();
                        this.commandeBoService.validateCommande(this.token, command.id,  data.Heure)
                            .subscribe((data: any) => {
                                this.loadCommands();
                                loader.dismiss();
                            });
                        //   this.goToTutorial();
                    }
                }]

            });

            alert.present();


        });
    }
}
