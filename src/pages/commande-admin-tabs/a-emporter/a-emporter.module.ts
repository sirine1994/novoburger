import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AEmporterPage } from './a-emporter';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    AEmporterPage,
  ],
  imports: [
    IonicPageModule.forChild(AEmporterPage),
      TranslateModule.forChild()
  ],
})
export class AEmporterPageModule {}
