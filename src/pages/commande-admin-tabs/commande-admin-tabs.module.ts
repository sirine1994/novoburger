import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommandeAdminTabsPage } from './commande-admin-tabs';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    CommandeAdminTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(CommandeAdminTabsPage),
      TranslateModule.forChild()
  ]
})
export class CommandeAdminTabsPageModule {}
