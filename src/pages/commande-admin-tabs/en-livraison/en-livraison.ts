import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController} from 'ionic-angular';
import {BoCommandesProvider} from '../../../providers/bo-commandes/bo-commandes';
import {UserProvider} from '../../../providers/user/user';
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the EnLivraisonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-en-livraison',
    templateUrl: 'en-livraison.html',
})
export class EnLivraisonPage {
    public event = {
        month: new Date().toISOString().split('T')[0],
        timeStarts: '07:43',
        timeEnds: '1990-02-20'
    };
    checked = false;
    commandesList: any[];
    token: string;
    first = 1;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public commandeBoService: BoCommandesProvider,
                public translate: TranslateService,
                public modalCtrl: ModalController,
                public userService: UserProvider,
                public loadingCtrl: LoadingController,
                private alertCtrl: AlertController) {
        this.token = this.userService.getUser().token;
        this.loadCommands();
    }

    ionViewDidEnter() {
        this.loadCommands();
    }

    private loadCommands() {
        this.translate.get(['loading_data', 'Operation_not_performed']).subscribe(res => {
            let loader = this.loadingCtrl.create({
                content: res.loading_data
            });
            loader.present();
            this.commandeBoService.getEn_LivraisonCommandes(this.token, this.event.month, this.first)
                .subscribe((data: any) => {
                        //
                        //
                        this.commandesList = data.data;

                        loader.dismiss();
                    },
                    err => {
                        loader.dismiss();
                        let alert = this.alertCtrl.create({
                            'message': res.Operation_not_performed
                        });
                        alert.present();
                    });
        });

    }

    ionViewWillLoad() {
    }

    ionViewDidLoad() {
        //
    }

    opendDetails(item) {

        let modal = this.modalCtrl.create("DetailCommandePage", {
            command: item
        });
        modal.present();
    }

    loadMore(infiniteScroll: any = null) {
        this.translate.get(['loading_data']).subscribe(res => {

            let loader = this.loadingCtrl.create({
                content: res.loading_data
            });
            loader.present();
            this.first += 1;


            this.commandeBoService.getEn_LivraisonCommandes(this.token, this.event.month, this.first)
                .subscribe((orderCommand: any) => {
                        // //
                        // //
                        orderCommand.data.forEach(x => {
                            this.commandesList.push(x);
                        });
                        if (infiniteScroll) {
                            infiniteScroll.complete();
                        }
                        loader.dismiss();
                    },
                    error => {
                        if (infiniteScroll != null) {
                            infiniteScroll.enable(false);
                        }
                    });
        });

    }

    getDayService(command: any) {
        //
        this.validateCommand(command.id);
    }

    toggleCommand() {
        this.checked = !this.checked;
    }

    dateChanged(event) {
        this.loadCommands();
    }

    modifyDateCommand(command: any) {
        this.translate.get(['fill_out_form', 'date_resv', 'annuler', 'send']).subscribe(res => {

            let alertt = this.alertCtrl.create({
                title: res.fill_out_form,
                inputs: [
                    {
                        label: 'Date',
                        name: 'Date',
                        type: 'date',
                        placeholder: res.date_resv
                    }
                ],
                buttons: [
                    {
                        text: res.annuler,
                        role: 'cancel',
                        handler: data => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: res.send,
                        handler: data => {

                            this.commandeBoService.dateModifCommande(this.token, command.id, data.Date)
                                .subscribe((data: any) => {
                                    this.loadCommands();

                                });
                        }
                    }
                ]
            });
            alertt.present();
        });

    }

    refuseCommand(command: any) {
        this.translate.get(['cancel_in_prog']).subscribe(res => {
            let loader = this.loadingCtrl.create({
                content: res.cancel_in_prog
            });
            loader.present();
            this.commandeBoService.refuseCommande(this.token, command.id)
                .subscribe((data: any) => {
                    this.loadCommands();
                    loader.dismiss();
                });
        });
    }

    acceptCommand(command: any) {
        this.translate.get(['accept_in_prog']).subscribe(res => {

            let loader = this.loadingCtrl.create({
                content: res.accept_in_prog
            });
            loader.present();
            this.commandeBoService.acceptCommande(this.token, command.id)
                .subscribe((data: any) => {

                    this.loadCommands();
                    loader.dismiss();
                });
        });
    }


    validateCommand(command: any) {
        this.translate.get(['valid_in_prog']).subscribe(res => {


            let alert = this.alertCtrl.create({
                title: '',
                subTitle: 'Facture commande',
                inputs: [
                    {
                        name: 'Heure',
                        type: 'time'
                    }
                ],
                buttons: [{
                    text: 'commande traitée', handler: () => {
                        let loader = this.loadingCtrl.create({
                            content: res.valid_in_prog
                        });
                        loader.present();
                        this.commandeBoService.validateCommande(this.token, command.id, command.Heure)
                            .subscribe((data: any) => {
                                this.loadCommands();
                                loader.dismiss();
                            });
                        //   this.goToTutorial();
                    }
                }]

            });

            alert.present();


        });

    }
}
