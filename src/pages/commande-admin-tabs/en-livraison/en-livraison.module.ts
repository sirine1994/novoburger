import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnLivraisonPage } from './en-livraison';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    EnLivraisonPage,
  ],
  imports: [
    IonicPageModule.forChild(EnLivraisonPage),
      TranslateModule.forChild()
  ],
})
export class EnLivraisonPageModule {}
