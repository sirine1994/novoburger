import {Component} from "@angular/core";
import {
    IonicPage,
    NavController,
    NavParams,
    ViewController,
    LoadingController, ModalController,
    ToastController, Events

} from "ionic-angular";
import {CurrencyProvider} from "../../providers/currency";
import {DomSanitizer} from "@angular/platform-browser";

import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {TranslateService} from "@ngx-translate/core";
import {RestGlobals} from "../../providers/rest-globals";

@IonicPage()
@Component({
    selector: "page-detail-food",
    templateUrl: "detail-food.html"
})
export class DetailFoodPage {
    base_url: any;
    list: any;
    list_only: any;
    list_ext: any;
    discount: any = 0;
    checkeds: number = 0;
    food_id: any;
    price: any;
    quantity: any;
    snug_size_name: any;
    snug_size_price: any = 0;
    snug_ext_id: any;
    snug_ext_name: any;
    snug_ext_price: any;
    total_price: any = '';
    temp_ext_id: any;
    select_size: any = '';
    showDiv1: boolean = false;
    showDiv2: boolean = false;
    showDiv3: boolean = false;
    showDiv4: boolean = false;
    showDiv5: boolean = false;
    panierLength: number = 0;
    settings: any = '';
    boissonSchoosen: number = 0;
    acpSchoosen: number = 0;
    dessertSchoosen: number = 0;
    saucesSchoosen: number = 0;
    food: any;
    loading: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public currencyProvider: CurrencyProvider,
        public viewCtrl: ViewController,
        public modalCtrl: ModalController,
        public sanitizer: DomSanitizer,
        public translate: TranslateService,
        public http: Http,
        public storage: Storage,
        public toastCtrl: ToastController,
        public events: Events
    ) {
        this.food = this.navParams.get("food");

        this.loading = this.navParams.get("");
        this.base_url = Constant.domainConfig.base_url;
        this.discount = this.navParams.get('discount');
        this.food_id = this.food.id;
        this.price = this.navParams.get("price");
        this.quantity = this.navParams.get("quantity");
        this.temp_ext_id = new Array();
        this.list = new Array();

        this.storage.get('settings').then(data => {
            this.settings = data;
        });

        // let myUrl='api/food_sizes_api/size?food_id='+this.food.id_food;
        /* if(typeof (this.food.id_menu)!= 'undefined'){

             let  myUrl='api/menu_api/get?store_id='+RestGlobals.idStore+'&categories_id='+this.food.categories_id+'&id='+this.food.id_menu;
             this.http.get(Constant.domainConfig.base_url+myUrl).subscribe(data=>{
                 if(data.json().empty == null){
                     this.list = data.json();
                 }else{
                     this.list = null;
                     this.total_price = (this.price*1 - this.price*this.discount/100) * this.quantity;
                     this.snug_size_name = 'default';
                     this.snug_size_price = this.total_price;
                 }
             });
         }*/
        this.list_ext = new Array();

        this.http.get(Constant.domainConfig.base_url + 'api/food_extras_api/extra?food_id=' + this.food.id).subscribe(data => {
            if (data.json().empty == null) {
                this.list_ext = data.json();
            } else {
                this.list_ext = null;
            }
        })
    }

    ionViewDidLoad() {
        this.food.quantity = 1;

        this.storage.get('carts').then((data) => {
            if (data) {
                this.panierLength = data.length;
            }
        });
    }

    btnhide(i) {

        if (i == 1) {

            this.showDiv1 = !(this.showDiv1);
            this.showDiv2 = false;
            this.showDiv3 = false;
            this.showDiv4 = false;
            this.showDiv5 = false;
        } else if (i == 2) {
            this.showDiv2 = !(this.showDiv2);
            this.showDiv1 = false;
            this.showDiv3 = false;
            this.showDiv4 = false;
            this.showDiv5 = false;
        } else if (i == 3) {
            this.showDiv3 = !(this.showDiv3);
            this.showDiv1 = false;
            this.showDiv2 = false;
            this.showDiv4 = false;
            this.showDiv5 = false;
        } else if (i == 4) {
            this.showDiv4 = !(this.showDiv4);
            this.showDiv1 = false;
            this.showDiv2 = false;
            this.showDiv3 = false;
            this.showDiv5 = false;
        } else if (i == 5) {

            this.showDiv5 = !(this.showDiv5);
            this.showDiv1 = false;
            this.showDiv2 = false;
            this.showDiv3 = false;
            this.showDiv4 = false;
        }
    }

    modalAddCart(item) {

        let modal = this.modalCtrl.create("AddCartPage", {
            food_id: item.id,
            discount: item.discount,
            price: item.price,
            quantity: item.quantity
        });
        modal.present();
    }

    cart() {
        this.navCtrl.push('CartPage')
    }

    accompChange(item) {

        /*let found = this.food.accompagenement.find(element => element.checked==true );
            if(found){
               for(let boi of this.food.boisson){
                  if(boi.checked) boi.checked=!boi.checked;

               }

           }*/

    }

    boiChange(item) {

        /*let found = this.food.boisson.find(element => element.checked==true );
        if(found){
            for(let boi of this.food.accompagenement){
                if(boi.checked) boi.checked=!boi.checked;

            }

        }*/
    }


    dismiss() {
        this.viewCtrl.dismiss();
    }

    minus() {
        if (this.food.quantity > 1) {
            this.food.quantity--;
        }
    }

    plus() {
        this.food.quantity++;
    }

    public

    getSafehtml(text) {
        var map = {
            '&amp;': '&',
            '&#038;': "&",
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'",
            '&#8217;': "’",
            '&#8216;': "‘",
            '&#8211;': "–",
            '&#8212;': "—",
            '&#8230;': "…",
            '&#8221;': '”'
        };

        return text.replace(/\&[\w\d\#]{2,5}\;/g, function (m) {
            return map[m];
        });


    }

    testIfSelected(mydata) {
        try {
        console.log(mydata);
            if (typeof mydata!="undefined" && mydata && mydata.length > 0)
                return mydata.filter((v) => {
                     console.log("v " + v);
                    if (v.checked) return true
                });
            return [];
        } catch (e) {
            return [];
        }
    }

    addToCart(food: any) {
        try {
            if (typeof this.food.sauces!="undefined" && this.testIfSelected(this.food.sauces).length == 0 && this.food && (this.food.sauces).length > 0) {
                this.toastMessage("Vous devez choisir une sauce");
                return;
            }
        } catch (e) {
            console.log(e);
        }

        if (this.food.boisson && this.food.boisson.length>0) {
            this.food.boisson[this.boissonSchoosen].checked = true;
        }

        if (this.food.accompagenement && this.food.accompagenement.length>0) this.food.accompagenement[this.acpSchoosen].checked = true;
        if (this.food.dessert && this.food.dessert.length > 0) this.food.dessert[this.dessertSchoosen].checked = true;
        this.translate.get(['err_select_size', 'added_cart']).subscribe(res => {
            if (this.list != null && this.select_size == '' && false) {
                //  alert(res.err_select_size);
            } else {

                this.storage.ready().then(() => {
                    this.storage.get('carts').then((data) => {
                        let carts = data;

                        let check = false;
                        //
                        /* for (var i in carts) {
                              // //
                              console.log("carts[i].lst_extras_id "+carts[i].lst_extras_id +" "+ food.id_food);
                              if (carts[i].lst_extras_id == food.id_food) {
                                  check = true;
                                  carts[i].quantity = carts[i].quantity + food.quantity;
                                  this.storage.set('carts', carts);
                                  //this.events.publish('user:add_cart');
                                  break;
                              }
                          }
                          ;

                          if (check == false) {*/
                        /* this.http.get(Constant.domainConfig.base_url + 'api/foods_api/foods?product_id=' + food.id_food)
                          .subscribe((obj: any) => {*/
                        let temp_obj = this.food;
                        temp_obj.quantity = food.quantity;
                        temp_obj.lst_extras_name = food.price;
                        if (typeof (food.id_food) != 'undefined')
                            temp_obj.lst_extras_id = food.id_food;
                        else
                            temp_obj.lst_extras_id = food.id_menu;
                        temp_obj.lst_extras_price = this.snug_ext_price;
                        temp_obj.size_name = food.name;
                        temp_obj.size_price = food.price;
                        temp_obj.total_price = food.price;
                        temp_obj.unite = food.unite;
                        temp_obj.boisson = this.food.boisson;
                        // temp_obj.accompagenement=new Array();
                        temp_obj.accompagenement = this.food.accompagenement;
                        temp_obj.dessert = this.food.dessert;
                        temp_obj.sauces = this.food.sauces;
                        temp_obj.supplements = this.food.supplements;
                        temp_obj.plat = this.food.plat;
                        this.storage.get('carts')
                            .then(x => {
                                let carts = x;
                                if (!carts)
                                    carts = new Array();
                                carts.push(temp_obj);
                                this.storage.set('carts', carts);
                            });


                        this.events.publish('user:add_cart');
                        // });
                        //  }
                        console.log("cartsss " + JSON.stringify(carts));
                    });
                });

                this.viewCtrl.dismiss();

                let toast = this.toastCtrl.create({
                    message: res.added_cart,
                    duration: 1000,
                    position: 'top'
                });
                toast.present();

            }

            return;
        });
    }

    toastMessage(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 1000,
            position: 'top'
        });
        toast.present();
    }

    check(entry) {
        if (entry.checked) {
            this.checkeds++;
            console.log(this.checkeds);
        } else {
            this.checkeds--;
            console.log(this.checkeds);
        }
    }

}
