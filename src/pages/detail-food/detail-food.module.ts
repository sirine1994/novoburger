import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailFoodPage } from './detail-food';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    DetailFoodPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailFoodPage),
      TranslateModule.forChild()
  ],
})
export class DetailFoodPageModule {}
