import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCardPage } from './add-card';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    AddCardPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCardPage),
      TranslateModule.forChild()
  ],
})
export class AddCardPageModule {}
