import { CodePromo } from './../../model/code-promo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { BoCommandesProvider } from '../../providers/bo-commandes/bo-commandes';
import { UserProvider } from '../../providers/user/user';
import { AddCodePromoPage } from './add-code-promo/add-code-promo';

/**
 * Generated class for the CodePrompBoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-code-promp-bo',
  templateUrl: 'code-promp-bo.html',
})
export class CodePrompBoPage {
  code_promoList:any[];
  token: string;
  first:number;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public commandeBoService: BoCommandesProvider,
              public userService:UserProvider,
              public boCommandeService: BoCommandesProvider,
              public modalController: ModalController,
              public loadingCtrl:LoadingController,
              private alertCtrl: AlertController) {
    this.first = 1;
  }

  ionViewDidLoad() {
    //
    this.token = this.userService.getUser().token;

    this.loadData(this.first);
  }

  private loadData(first:number){
    
    let loader = this.loadingCtrl.create({
      content: 'Chargement de données'
    });
    loader.present();
    this.commandeBoService.getCodePromos(this.token, first)
      .subscribe((data: any) => {
        this.code_promoList = data.data;
        //

        loader.dismiss();
      },
			err=>{
        loader.dismiss();
				let alert=this.alertCtrl.create({
					'message':'Operation non effectué'
				  });
				  alert.present();
			});
  }

  loadMore(infiniteScroll:any=null){
    
    this.first+=1;


    this.commandeBoService.getCodePromos(this.token ,this.first)
    .subscribe((orderCommand:any) =>{
      // //
      // //
      orderCommand.data.forEach(x=>{
        this.code_promoList.push(x);
      })
      if(infiniteScroll){
        infiniteScroll.complete();
      }
    },
    error =>{
      if(infiniteScroll!=null){
        infiniteScroll.enable(false);
      }
    });
  }

  addCodePromo(){
    let modal = this.modalController.create(
      'AddCodePromoPage',
      {
        token:this.token
      }
    );

    modal.present();

    modal.onDidDismiss((data)=>{
      //
      this.loadData(1);
    });
  }

  updateCodePromo(event,codepromo:CodePromo){
    //
    //

    let modal = this.modalController.create(
      'AddCodePromoPage',
      {
        token:this.token,
        codePromo: codepromo
      }
    );

    modal.present();

    modal.onDidDismiss((data)=>{
      //
      this.loadData(1);
    });
  }
}
