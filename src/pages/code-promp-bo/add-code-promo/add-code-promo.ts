import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { CodePromo } from '../../../model/code-promo';
import { BoCommandesProvider } from '../../../providers/bo-commandes/bo-commandes';
import { UserProvider } from '../../../providers/user/user';



@IonicPage()
@Component({
  selector: 'page-add-code-promo',
  templateUrl: 'add-code-promo.html',
})
export class AddCodePromoPage {
  public event = {
    dateStart: new Date(),
    dateEnd: new Date(),
  }
  token:string;
  codePromo:CodePromo;
  constructor(public view:ViewController,
              public navCtrl: NavController, 
              public navParams: NavParams,
              public boCommandesService: BoCommandesProvider,
              public userService:UserProvider,
              public loadingCtrl:LoadingController,
              private alertCtrl: AlertController) {
  }

  ionViewWillLoad() {
    
    this.token=this.navParams.get('token');
    if(this.navParams.get('codePromo')){
      this.codePromo =this.navParams.get('codePromo');
    }else{
      this.codePromo = new CodePromo();
    }
    //
  }
  ionViewDidLoad() {
    //
  }
  closePage(){
    const data = {
      name: 'Ahmed Schhaider',
      occupation: 'Developer'
    }

    this.view.dismiss(data);
  }

  addCodePromo(){
    
    let loader = this.loadingCtrl.create({
      content: 'Ajout de code promo'
    });
    loader.present();
    this.boCommandesService.add_codePromo(this.userService.getUser().token, this.codePromo)
      .subscribe((data:any)=>{
        //
        this.view.dismiss(data);

        loader.dismiss();
      },
			err=>{
				loader.dismiss();
				let alert=this.alertCtrl.create({
					'message':'Operation non effectué'
				  });
				  alert.present();
			});
  }
}
