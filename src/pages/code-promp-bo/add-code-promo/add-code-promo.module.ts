import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCodePromoPage } from './add-code-promo';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    AddCodePromoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCodePromoPage),
      TranslateModule.forChild()
  ],
  exports:[
    AddCodePromoPage
  ]
})
export class AddCodePromoPageModule {}
