import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodePrompBoPage } from './code-promp-bo';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    CodePrompBoPage,
  ],
  imports: [
    IonicPageModule.forChild(CodePrompBoPage),
      TranslateModule.forChild()
  ],
})
export class CodePrompBoPageModule {}
