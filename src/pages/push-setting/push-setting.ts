import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {FcmProvider} from '../../providers/fcm/fcm';

 @IonicPage()
 @Component({
 	selector: 'page-push-setting',
 	templateUrl: 'push-setting.html',
 })
 export class PushSettingPage {
 	toggle:any;
 	constructor(public navCtrl: NavController,
 		public storage:Storage,
 		public fcmService: FcmProvider,
 		public navParams: NavParams,
 		public viewCtrl: ViewController) {
 		this.storage.get('enable_push').then(val=>{
 			this.toggle=val;
 		})
 	}

 	ionViewDidLoad() {
 		// //
 	}

 	togglePush(){
 		this.storage.get('enable_push').then(val=>{
 			if(val==false){
				 this.storage.set('enable_push',true);
				 this.fcmService.setSubscription(true);
 			}else{
 				this.storage.set('enable_push',false);
				 this.fcmService.setSubscription(false);
 			}
 		})
 	}

 	dismiss(){
 		this.viewCtrl.dismiss();
 	}

 }
