import {
    IonicPage, NavController, Slides, NavParams, LoadingController, ToastController,
    ModalController, ViewController
} from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {CallNumber} from '@ionic-native/call-number';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {TranslateService} from "@ngx-translate/core";
import {RestGlobals} from "../../providers/rest-globals";
import {UserProvider} from "../../providers/user/user";
import {Component, Input, ViewChild} from '@angular/core';
import {CurrencyProvider} from "../../providers/currency";

@IonicPage()
@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html'
})
export class CategoriesPage {
    @ViewChild('titleSlides') titleSlides: Slides;
    titleSlideSelectedIndex: number = 0;
    list: any;
    publicite: any;
    listInitial: any;
    listFoodInitial: any;
    first: number = 0;
    panierLength:number=0;
    listFood: any;
    base_url: any;
    total_card: any = 0;
    settings: any = '';
    loading: any;
    isSerch: boolean = false;
    previousLoading: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: Http,
        public callNumber: CallNumber,
        public loadingCtrl: LoadingController,
        public translate: TranslateService,
        public viewCtrl: ViewController,
        public iab: InAppBrowser,
        public toastCtrl: ToastController,
        public userService: UserProvider,
        public currencyProvider: CurrencyProvider,
        public modalCtrl: ModalController,
        public storage: Storage) {
        this.base_url = Constant.domainConfig.base_url;
        this.listFood = new Array();
        this.publicite = new Array();
        this.listFoodInitial = new Array();
        this.previousLoading = this.navParams.get("loading");
        this.http.get(Constant.domainConfig.base_url + 'api/categories_api/categories?store_id=' + RestGlobals.idStore).subscribe(data => {
            this.list = data.json();
            this.listInitial = new Array();
            this.listInitial = data.json();
            this.changeSliderPosition(0);

        });


        this.storage.ready().then(() => {
            this.storage.get('settings').then(data => {
                this.settings = data;
            })
        })
    }

    ionClear() {
        this.list = JSON.parse(JSON.stringify(this.listInitial));
    }

    public getSafehtml(text) {
        var map = {
            '&amp;': '&',
            '&#038;': "&",
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'",
            '&#8217;': "’",
            '&#8216;': "‘",
            '&#8211;': "–",
            '&#8212;': "—",
            '&#8230;': "…",
            '&#8221;': '”'
        };

        return text.replace(/\&[\w\d\#]{2,5}\;/g, function (m) {
            return map[m];
        });


    }

    ngAfterViewInit() {

        this.titleSlides.freeMode = true;
        this.titleSlides.slidesPerView = 3;
    }

    ionViewDidLoad() {
        RestGlobals.lastPage = "CategoriesPage";
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        this.http
            .get(Constant.domainConfig.base_url + "api/publicity_api/show?store_id=" + RestGlobals.idStore, {
                headers: headers
            })
            .subscribe(
                (data: any) => {
                   if(data.json().publicity){
this.publicite = data.json().publicity[0];
                    
                    this.modalPub(this.publicite);
                   } 

                                    },
                error => {
                    //this.loading.dismiss();
                }
            );

        this.storage.get('carts').then((data) => {
            if(data){this.panierLength=data.length;
            }
        });
    }

    ionViewWillEnter() {
        this.storage.get('carts').then((obj) => {
            if (obj) {
                this.total_card = obj.length;
                this.panierLength=obj.length;
            }
            if (this.previousLoading) {
                this.previousLoading.dismiss();
            }
        });
    }

    modalPub(pub) {
        let modal = this.modalCtrl.create("PublicitePage", {
            pub: pub
        });
           
        modal.present();
    }

    modalDatailFood(item) {
        let modal = this.modalCtrl.create("DetailFoodPage", {
            food: item
        });
        modal.onDidDismiss(() => {
            this.storage.get('carts').then((data) => {
                if(data){this.panierLength=data.length;
                    console.log(data.length);}
            });
        });
        modal.present();
    }

    goback() {
        this.viewCtrl.dismiss();
    }

    cart() {
        this.navCtrl.push('CartPage')
    }

    openPage(item) {
        this.translate.get(['loading', 'no_product']).subscribe(res => {
            if (item.total > 0) {
                this.loading = this.loadingCtrl.create({
                    content: res.loading
                });
                this.loading.present();
                this.navCtrl.push('FoodListPage', {id: item.id_cat, name: item.name_cat, loading: this.loading});
            } else {
                let toast = this.toastCtrl.create({
                    message: res.no_product,
                    duration: 1000,
                    position: 'buttom'
                });
                toast.present();
            }
        });
    }

    login() {
        if (!this.userService.getUser()) this.navCtrl.setRoot("LoginPage");
    }

    openCartPage() {
        this.translate.get(['loading_data']).subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.loading_data
            });
        });
        this.loading.present();
        this.navCtrl.setRoot('CartPage', {loading: this.loading});
    }

    search() {
        this.isSerch = !this.isSerch;
    }

    changeSliderPosition(currentIndex) {

        this.titleSlideSelectedIndex = currentIndex;


        this.titleSlides.slideTo(this.titleSlideSelectedIndex, 500);
        console.log('titleSlideSelectedIndex:' + this.titleSlideSelectedIndex);
        console.log(this.list[currentIndex].id_cat);
        this.listFood = new Array();
        this.first = 0;
        this.first += 1;
        this.http.get(
            this.base_url + 'api/foods_api/all_food_menu' +
            //'?token=' + this.userService.getUser().token +
            '?categories_id=' + this.list[currentIndex].id_cat +
            '&store_id=' + RestGlobals.idStore +
            '&first=' + this.first /* +
        '&offset=' + 5  */
        ).subscribe((data: any) => {
            console.log(JSON.stringify(data));
            var jsonData = JSON.parse(data._body);
            //
            console.log(JSON.stringify(jsonData));
            jsonData.forEach(element => {
                this.listFood.push(element);
                //this.listFoodInitial.push(element);
            });
        });
    }

    searchBook(searchbar) {
        // reset countries list with initial call
        this.list = JSON.parse(JSON.stringify(this.listInitial));
        var q = searchbar.target.value;

        // if the value is an empty string don't filter the items
        if (q.trim() == '') {
            return;
        }

        this.list = this.list.filter((v) => {
            console.log("vvv " + JSON.stringify(v));
            //   return (v.toLowerCase().indexOf(q.toLowerCase()) > -1)
            return (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1)
        });

    }

    facebook() {
        let browser = this.iab.create(this.settings.facebook, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    twitter() {
        let browser = this.iab.create(this.settings.twitter, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    call() {
        this.callNumber.callNumber(this.settings.phone, true);
    }

}
