import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoreCardPage } from './store-card';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    StoreCardPage,
  ],
  imports: [
    IonicPageModule.forChild(StoreCardPage),
      TranslateModule.forChild()
  ],
  exports: [StoreCardPage]
})
export class StoreCardPageModule {}
