import {UserProvider} from "./../../providers/user/user";
import {Component, ElementRef, ViewChild} from "@angular/core";
import {
    AlertController,
    IonicPage,
    Loading,
    LoadingController,
    NavController,
    NavParams,
    ViewController,
    ToastController
} from "ionic-angular";
import * as Constant from "../../config/constants";
import {NgForm} from "@angular/forms";
import {stripe_publish_key} from "../../config/constants";
import {RestGlobals} from "../../providers/rest-globals";
import {Storage} from '@ionic/storage';
import {Http, Headers} from "@angular/http";
import {HomePage} from "../home/home";
import {TranslateService} from "@ngx-translate/core";
import {CategoriesPage} from "../categories/categories";
// import {RestGlobals} from "../../providers/rest-globals";
// import {Shared} from "../../providers/shared";
// import {SubscriptionsApi} from "../../providers/apis/subscriptions";
// import {UsersApi} from "../../providers/apis/users";

declare var stripe: any;
declare var elements: any;

/**
 * Generated class for the StoreCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: "page-store-card",
    templateUrl: "store-card.html"
})
export class StoreCardPage {
    @ViewChild('hiddenInput') hiddenInput;
    loading: Loading;
    card: any;
    myCardData: any;
    error: string;
    setupToken: string;
    panierLength: number = 0;
    errorMessage: string = "";
    @ViewChild("cardInfo") cardInfo: ElementRef;

    disabled: boolean = true;

    isCreteStore: boolean = false;

    card_info = {
        number: '4242424242424242',
        expMonth: 12,
        expYear: 2020,
        cvc: '220'
    };

    constructor(public navCtrl: NavController,
                public storesApi: UserProvider,
                public translate: TranslateService,
                public alertCtrl: AlertController,
                public viewCtrl: ViewController,
                public usersApi: UserProvider,
                public navParams: NavParams,
                public storage: Storage,
                public loadingCtrl: LoadingController,
                public toastCtrl: ToastController,
                public http: Http,) {
    }

    cart() {
        this.navCtrl.push('CartPage')
    }

    ionViewWillLeave() {
        try {
            this.hiddenInput.setFocus();
            this.hiddenInput.setBlur();
            if (this.card) this.card.destroy();

        } catch(e) {
        }
    }

    ionViewDidLoad() {

        if (this.card) this.card.destroy();
        this.getInfo();
        this.getSetup();
        var elementStyles = {
            base: {
                color: '#00ff00',
                background: '#fff',
                fontWeight: 600,
                fontFamily: 'Quicksand, Open Sans, Segoe UI, sans-serif',
                fontSize: '16px',
                fontSmoothing: 'antialiased',


                ':focus': {
                    color: '#000',
                    background: '#fff',
                },

                '::placeholder': {
                    color: '#000',
                    background: '#fff',
                },

                ':focus::placeholder': {
                    color: '#000',
                    background: '#fff',
                },
            },
            invalid: {
                color: '#00ff00',
                background: '#fff',
                ':focus': {
                    color: '#FA755A',
                    background: '#fff',
                },
                '::placeholder': {
                    color: '#FFCCA5',
                    background: '#fff',
                },
            },
        };

        var elementClasses = {
            focus: 'focus',
            empty: 'empty',
            invalid: 'invalid',
        };

        this.card = elements.create('card', {
            style: elementStyles,
            classes: elementClasses
        });

        this.card.mount(this.cardInfo.nativeElement);
        this.storage.get('carts').then((data) => {
            if (data) {
                this.panierLength = data.length;
            }
        });

    }

    ionViewDidLeave() {

        try {
            // this.card.unmount();
            // this.card=null;
        } catch (e) {

        }
    }

    ionViewDidEnter() {
    }

    pay(form: NgForm) {
        this.translate.get(['loadingC']).subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.loadingC
            });
        });
        this.loading.present();

        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });

        try {
            stripe.handleCardSetup(this.setupToken, this.card, {payment_method_data: {billing_details: {name: "testingcard"}}}).then((result) => {
                    if (result.error) {
                        this.loading.dismissAll();
                    } else {

                        var data;
                        if (result.setupIntent.payment_method !== null)
                            data =
                                "token=" + this.usersApi.getUser().token + "&token_card=" + result.setupIntent.payment_method;
                        //
                        this.translate.get(['card_saved', 'err_stripe']).subscribe(res => {
                            if (result.setupIntent.payment_method !== null)
                                this.http.post(
                                    Constant.domainConfig.base_url + "api/stripe_api/create_card",
                                    data,
                                    {headers: headers}
                                ).subscribe(
                                    data => {
                                        this.loading.dismissAll();
                                        let toast = this.toastCtrl.create({
                                            message: res.card_saved,
                                            duration: 3000,
                                            position: "top"
                                        });
                                        toast.present();
                                    },
                                    error => {
                                        this.loading.dismissAll();
                                        let toast = this.toastCtrl.create({
                                            message: res.err_stripe,
                                            duration: 3000,
                                            position: "top"
                                        });
                                        toast.present();
                                    }
                                );
                        });
                    }
                }
            );
        } catch (e) {
            this.loading.dismissAll();
        }


        /*
          this.stripe.getCardType(this.card_info.number)
            .then(data =>{
              //
              //
            }) */
    }

    goCommander() {
        // alert(RestGlobals.idStore);

        if(RestGlobals.idStore && typeof(RestGlobals.idStore)!='undefined'){
            if (RestGlobals.lastPage != '')
               this.navCtrl.pop();//  this.navCtrl.setRoot(RestGlobals.lastPage);
            else this.navCtrl.setRoot('CategoriesPage');
        } else
            this.navCtrl.setRoot('searchStorePage');

    }

    public createCard(token) {
        //
        this.loading = this.loadingCtrl.create({
            spinner: "hide",
            content:
                '  <div class="custom-spinner-box"> <img src="assets/images/chargemen11t.gif"/> </div>'
        });
        this.loading.present();

        this.usersApi.setCard(stripe_publish_key, token);
        /* .subscribe(
                    data => this.wsSuccessCard(data),
                    error => this.wsFailure(<any>error)) */
    }

    wsSuccessCard(data) {
        this.errorMessage = null;
        this.loading.dismissAll();
        if (data.stripe_message_error == null) {
            if (data) {
                //alert(data.customer_token);
                this.disabled = false;
            }
        } else {
            this.errorMessage = data.stripe_message_error;
        }
    }

    private wsFailure(err) {
        //
        this.translate.get(['err_server', 'ok']).subscribe(res => {
            this.loading.dismissAll();
            let actionSheet = this.alertCtrl.create({
                title: res.err_server,
                buttons: [
                    {
                        text: res.ok,
                        role: "cancel"
                    }
                ],
                enableBackdropDismiss: false
            });
            actionSheet.present();
        });
    }

    goBack() {

        if (this.navCtrl.length() > 1) {
            let data = this.navParams.get("data");
            data.type_Paiement = "par_carte";
            this.navCtrl.pop();
             //this.navCtrl.push('CheckoutPage', {"data": data, "livraison": 2});
        } else
            this.navCtrl.setRoot('searchStorePage');
        //this.viewCtrl.dismiss();
    }


    getSetup() {
        this.translate.get(['loadingC']).subscribe(res => {

            this.loading = this.loadingCtrl.create({
                content: res.loadingC
                /* spinner: 'hide',
                      content: '  <div class="custom-spinner-box"> <img src="assets/images/chargemen11t.gif"></img> </div>' */
            });
        });
        this.loading.present();

        this.storesApi.getStripeSetup(this.usersApi.getUser().token).subscribe(
            data => this.wsSuccessSetupGet(data),
            error => this.wsFailure(<any>error));
    }


    getInfo() {
        this.translate.get(['wait']).subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.wait
                /* spinner: 'hide',
                      content: '  <div class="custom-spinner-box"> <img src="assets/images/chargemen11t.gif"></img> </div>' */
            });
        });
        //this.loading.present();

        this.storesApi.getInfoStripeCard(this.usersApi.getUser().token).subscribe(
            data => this.wsSuccessCardGet(data),
            error => this.wsFailure(<any>error));
    }

    wsSuccessCardGet(data) {
        this.loading.dismissAll();
        if (data.card != null) {
            this.myCardData = data.card;

        }

    }

    wsSuccessSetupGet(data) {
        this.loading.dismissAll();
        this.setupToken = data.setup_intent.client_secret;
    }
}
