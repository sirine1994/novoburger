import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { TranslateModule } from '@ngx-translate/core';
//import {AgmCoreModule} from "@agm/core";
import {google_api_key} from "../../config/constants";


@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    TranslateModule.forChild()

  ],
  exports: [
    ProfilePage
  ]
})
export class ProfilePageModule {}
