import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Component, ViewChild, NgModule, NgZone} from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http, Headers } from '@angular/http';
import  * as Constant from '../../config/constants';
import { Storage } from '@ionic/storage';


import {TranslateService} from '@ngx-translate/core';
import { Events } from 'ionic-angular';
import {RestGlobals} from "../../providers/rest-globals";
import {UserProvider} from "../../providers/user/user";
//import {MapsAPILoader} from "@agm/core";
// import { OneSignal } from '@ionic-native/onesignal';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  base_url: any='';
  user: any='';
  user_id: any='';
    myaddress:string;
  avatar: any;
  full_name: any;
    user_name:string;
    genre:string;
  phone: any;
  email:any;
  address: any;
  old_pwd: any;
  new_pwd: any;
  confirm_pwd: any;
  check_edit: any=false;
  msg_err_edit: any;
  msg_err_pwd: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    public http: Http,
   public toastCtrl:ToastController,
   // private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public events: Events,
    public userService: UserProvider,
    public storage: Storage,
    public translate: TranslateService,
  ) {
    this.base_url = Constant.domainConfig.base_url;
      this.http.get(Constant.domainConfig.base_url+'api/adresse_api/get?token='+this.userService.getUser().token ).subscribe(data=>{
          this.myaddress=data.json().mes_adresses[0].adr+' '+data.json().mes_adresses[0].complement;
      });
    this.events.subscribe('user: change', () => {
       this.ionViewWillEnter();
    });
  }

  ionViewWillEnter(){
      /*this.mapsAPILoader.load().then(() => {
          let nativeHomeInputBox = document
              .getElementById("txtAdresse")
              .getElementsByTagName("input")[0];
          let autocomplete = new google.maps.places.Autocomplete(
              nativeHomeInputBox,
              {
                  types: ["address"],
                  componentRestrictions:{country:"fr"}
              }
          );
          let actualAdresse: string;
          autocomplete.addListener("place_changed", () => {
              this.ngZone.run(() => {
                  //get the place result
                  let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                  //verify result
                  this.address = place.formatted_address;
                  if (place.geometry === undefined || place.geometry === null) {
                      return;
                  }

                  //set latitude, longitude and zoom

              });
          });
      });*/
      this.storage.get('user').then((obj) => {
      // //
      this.user = new Array;
      if (obj == null) {
        this.user = null;
      }else{
        this.user = obj;
        this.user_id = obj.id;
        this.full_name = obj.full_name;
        this.genre = obj.genre;
        this.user_name= obj.user_name;
        this.phone = obj.phone;
        this.email = obj.email;
        this.address = obj.address;
      }
    });
  }
    cart(){
        this.navCtrl.push('CartPage')
    }

  update_profile(){
      this.translate.get(['err_msg_edit','success_update_profil']).subscribe(res => {

          let reg = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
          if (
              this.full_name.length >= 5 &&
              this.full_name.length <= 60 &&
              reg.test(this.phone) == true
          ) {
              let headers: Headers = new Headers({
                  'Content-Type': 'application/x-www-form-urlencoded'
              });
              this.http.post(this.base_url + 'api/users_api/update',
                  {
                      id: this.user_id,
                      full_name: this.full_name,
                      genre: this.genre,
                      user_name: this.user_name,
                      phone: this.phone,
                      address:'',
                      token: this.user.token
                  }
                  , {headers: headers})
                  .subscribe(data => {


                      if (data.json().ok == 0) {
                          this.msg_err_edit = res.err_msg_edit;
                          this.toastMessage(this.msg_err_edit);
                      } else {
                          let user = data.json()[0];
                            let password="";
                      this.storage.get("user").then(obj => {
                   
                          this.storage.remove('user').then(success => {
                            
  this.storage.set("user", user);
                           this.userService.setUser(user);
                          
                            this.events.publish("user: login");
                          });
                      });
                        
                          this.msg_err_edit = res.success_update_profil;
                          this.toastMessage(this.msg_err_edit);
                      }
                  }, error => {
                  });
          } else {
              this.msg_err_edit = res.err_msg_edit;
              this.toastMessage(this.msg_err_edit);
          }
      });
  }
    toastMessage(txt){
        let toast = this.toastCtrl.create({
            message:txt,
            duration:1000,
            position:'buttom'
        });
        toast.present();
    }
  change_pwd(){
    let reg = /^[a-zA-Z0-9]+$/;
      this.translate.get(['pwd_changed','err_login','check_data_insert']).subscribe(res => {

          if (this.old_pwd != null && this.new_pwd != null && this.new_pwd == this.confirm_pwd && this.new_pwd.length <= 60 && this.new_pwd.length >= 5 && reg.test(this.new_pwd) == true) {
              let headers: Headers = new Headers({
                  'Content-Type': 'application/x-www-form-urlencoded'
              });
              this.http.post(this.base_url + 'api/users_api/pwd', 'id=' + this.user_id + '&old_pass=' + this.old_pwd + '&new_pass=' + this.new_pwd, {headers: headers}).subscribe(data => {
                  // //
                  if (data.json().ok == 1) {
                      this.msg_err_pwd = res.pwd_changed;
                      this.old_pwd = null;
                      this.new_pwd = null;
                      this.confirm_pwd = null;
                  } else {
                      this.msg_err_pwd = res.err_login;
                  }
              }, error => {

              })
          } else {
              this.msg_err_pwd = res.check_data_insert;
          }
      });
  }

}
