import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the PublicitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-publicite',
  templateUrl: 'publicite.html',
})
export class PublicitePage {
  pub:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController) {
    this.pub = this.navParams.get("pub");
  }

  ionViewDidLoad() {
    this.pub = this.navParams.get("pub");
    

    console.log('ionViewDidLoad PublicitePage');
  }
  close() {
    this.viewCtrl.dismiss();
  }
}
