import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
/**
 * Generated class for the MentionLigalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mention-ligales',
  templateUrl: 'mention-ligales.html',
})
export class MentionLigalesPage {
type:string;
content:string;
  constructor(public navCtrl: NavController, public navParams: NavParams , public http: Http) {
 this.type=this.navParams.get('type');
 

  }

  ionViewDidLoad() {
    let headers: Headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        
       
            this.http.get(Constant.domainConfig.base_url + 'api/loi_api/get?type='+this.type, {headers: headers}).subscribe(data => {
                if (data.json() != null) {
                    console.log("data.json()"+data.json().loi[0].name);
                    this.content=data.json().loi[0].name;
            }});
  }

}
