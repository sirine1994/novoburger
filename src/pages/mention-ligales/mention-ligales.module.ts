import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MentionLigalesPage } from './mention-ligales';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    MentionLigalesPage,
  ],
  imports: [
    IonicPageModule.forChild(MentionLigalesPage),
      TranslateModule.forChild()
  ],
})
export class MentionLigalesPageModule {}
