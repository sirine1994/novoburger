import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LivraisonPage } from './livraison';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    LivraisonPage,
  ],
  imports: [
    IonicPageModule.forChild(LivraisonPage),
      TranslateModule.forChild()
  ],
})
export class LivraisonPageModule {}
