import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import * as Constant from "../../config/constants";
import {Headers, Http} from "@angular/http";
import {UserProvider} from "../../providers/user/user";

import {NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult} from "@ionic-native/native-geocoder";

/**
 * Generated class for the EditAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-edit-address',
  templateUrl: 'edit-address.html',
})
export class EditAddressPage {
    foptions: number;
    test: boolean;
    type_address:string;
    name: string;
    myAdresse:string;
    comp:string;
    autocomplete: any;
    GoogleAutocomplete: any;
    lat: number = 51.678418;
    lng: number = 7.809007;
mesAdr:any;
    autocompleteItems: any;
    constructor(public navCtrl: NavController,
                public geocoder: NativeGeocoder,
                public userService:UserProvider,
                public http: Http,
                public navParams: NavParams,public viewCtrl:ViewController) {
    this.type_address="Chez moi";
    this.name="chez-moi";
      if(this.name=="chez-moi") this.typeAddress(1);
      else if(this.name=="bureau") this.typeAddress(2);
      else if(this.name=="aucun") this.typeAddress(3);
      else if(this.name=="autre") this.typeAddress(4);
      this.test = true;
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.autocomplete = {
            input: ''
        };
        this.autocompleteItems = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditAddressPage');
  this.mesAdr=this.navParams.get('list')}
    dismiss() {
        this.viewCtrl.dismiss();
    }
    addMyAdress(){
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });

let data={token:this.userService.getUser().token,lat:this.lat,lng:this.lng,adr:this.myAdresse,complement:this.comp,title_adr:this.type_address};
        this.http
            .post(Constant.domainConfig.base_url + "api/adresse_api/add", JSON.stringify(data), {
                headers: headers
            })
            .subscribe(
                (data: any) => {
this.dismiss();
                },
                error => {

                }
            );

    }


    updateSearchResults() {
        let config = {
            types: ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: this.autocomplete.input,
            componentRestrictions: {country: 'FR'}
        };
        if (this.autocomplete.input == '') {

            this.autocompleteItems = [];
            return;
        }

        this.GoogleAutocomplete.getPlacePredictions(config,
            (predictions, status) => {
                this.autocompleteItems = [];
                if (predictions) {
                    //this.zone.run(() => {
                    predictions.forEach((prediction) => {

                        this.autocompleteItems.push(prediction);
                    });
                    // });
                }
            });
    }

    selectSearchResult(item) {
        this.autocompleteItems = [];


        // this.SearchForm.controls.codePostal.setValue(item.description);


        this.autocomplete.input = item.description;
        this.myAdresse = item.description;
        this.lat = 43.5844542;
        this.lng = 1.4476568;
        //this.calculateDistance(this.lat, this.lng);
        this.geocoder.forwardGeocode(item.description)
            .then((coordinates: NativeGeocoderForwardResult[]) => {

                    this.lat = Number(coordinates[0].latitude);

                    this.lng = Number(coordinates[0].longitude);

                    //this.calculateDistance(this.lat, this.lng);


                }
            )
            .catch((error: any) => console.log(error)
            );


    }







    selectOption(val) {
        //  this.moyen_transport = val1;

        //this.name=val2;
        this.foptions = val;
        this.test = true;
    }
    optionSelected(val): boolean {
        if (this.foptions == val) {
            return true;
        }
        else
            return false;

    }
    typeAddress(val) {
        // this.moyen_transport=val;

        if (val == 1) {
            this.name = "chez-moi";
            this.type_address = "Chez moi";
        }
        else if (val == 2) {
            this.name = "bureau";
            this.type_address = "Bureau";

        }
       /* else if (val == 3) {
            this.name = "aucun";
            this.type_address = "Aucun";

        }*/
        else if (val == 4) {
            this.name = "autre";
            this.type_address = "Autre";

        }


        this.selectOption(2);
    }
}
