import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LivraisonPointRetraitPage } from './livraison-point-retrait';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    LivraisonPointRetraitPage,
  ],
  imports: [
    IonicPageModule.forChild(LivraisonPointRetraitPage),
      TranslateModule.forChild()
  ],
})
export class LivraisonPointRetraitPageModule {}
