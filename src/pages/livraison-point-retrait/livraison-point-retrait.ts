import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the LivraisonPointRetraitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-livraison-point-retrait',
    templateUrl: 'livraison-point-retrait.html',
})
export class LivraisonPointRetraitPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LivraisonPointRetraitPage');
    }

    livraison() {
        this.navCtrl.push('LivraisonPage');

    }

    pointR() {
        this.navCtrl.push('PointRetraitPage');

    }
}
