import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaimentCardsPage } from './paiment-cards';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    PaimentCardsPage,
  ],
  imports: [
    IonicPageModule.forChild(PaimentCardsPage),
      TranslateModule.forChild()
  ],
})
export class PaimentCardsPageModule {}
