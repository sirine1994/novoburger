import { Component } from '@angular/core';
import {
    AlertController, IonicPage, Loading, LoadingController, NavController, NavParams,
    ViewController
} from 'ionic-angular';
import {AddCardPage} from "../add-card/add-card";
import {StoreCardPage} from "../store-card/store-card";
import {UserProvider} from "../../providers/user/user";
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the PaimentCardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paiment-cards',
  templateUrl: 'paiment-cards.html',
})
export class PaimentCardsPage {
    loading: Loading;

    myCardData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public translate:TranslateService
    ,public storesApi: UserProvider,public usersApi: UserProvider,
   public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    
  }

    addCard(){

        this.navCtrl.push('StoreCardPage');

       // this.navCtrl.setRoot('AddCardPage');
 }

    private wsFailure(err){
        //
        this.loading.dismissAll();

    }

    getInfo(){
      this.translate.get(['loadingC']).subscribe(res => {
        this.loading = this.loadingCtrl.create({
            content: res.loadingC
            /* spinner: 'hide',
                  content: '  <div class="custom-spinner-box"> <img src="assets/images/chargemen11t.gif"></img> </div>' */
        });
        this.loading.present();

        this.storesApi.getInfoStripeCard(this.usersApi.getUser().token).subscribe(
            data => this.wsSuccessCardGet(data),
            error => this.wsFailure(<any>error)) ;
    });
  }

    wsSuccessCardGet(data){
        this.loading.dismissAll();
        if(data.card!=null){
            this.myCardData = data.card;

        }

    }
}
