import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailCommandePage } from './detail-Commande';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    DetailCommandePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailCommandePage),
      TranslateModule.forChild()
  ],
})
export class DetailCommandePageModule {}
