import {Component} from "@angular/core";
import {
    IonicPage,
    NavController,
    NavParams,
    ViewController,
    LoadingController, ModalController,
    ToastController, Events, AlertController, Loading

} from "ionic-angular";
import {CurrencyProvider} from "../../providers/currency";
import {DomSanitizer} from "@angular/platform-browser";
import {CommandesClientProvider} from './../../providers/commandes-client/commandes-client';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {round} from "ionic-angular/umd/components/slides/swiper/swiper-utils";
import {UserProvider} from "../../providers/user/user";

@IonicPage()
@Component({
    selector: "page-detail-Commande",
    templateUrl: "detail-Commande.html"
})
export class DetailCommandePage {
    base_url: any;
    list: any;
    list_only: any;
    list_ext: any;
    discount: any = 0;
    food_id: any;
    price: any;
    quantity: any;
    snug_size_name: any;
    snug_size_price: any = 0;
    snug_ext_id: any;
    snug_ext_name: any;
    snug_ext_price: any;
    total_price: any = '';
    temp_ext_id: any;
    panierLength: number = 0;
    select_size: any = '';
    settings: any = '';
    canRefuse: boolean = false;

    food: any;
    loading: any;
    store: string;
    myCardData: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public currencyProvider: CurrencyProvider,
        public viewCtrl: ViewController,
        public modalCtrl: ModalController,
        public sanitizer: DomSanitizer,
        public http: Http,
        public storage: Storage,
        public toastCtrl: ToastController,
        public commandeClientsService: CommandesClientProvider,
        public events: Events, public loadingCtrl: LoadingController,
        public storesApi: UserProvider, public usersApi: UserProvider, public alertCtrl: AlertController
    ) {
        this.food = new Array();
    }

    ionViewDidLoad() {
        let idcmd = this.navParams.get("command").cmd.id;
        this.store = this.navParams.get("command").cmd.store;

        this.commandeClientsService.getCommandesInfo(idcmd)
            .subscribe((orderClient: any) => {
                // //
                this.food = orderClient.data[0];
                let arrDat = this.food.cmd.delivery_date.split("-");
                this.canRefuse = new Date(arrDat[2] + "-" + arrDat[1] + "-" + arrDat[0] + "T" + this.food.cmd.heure).getTime() > new Date().getTime() && this.food.cmd.status == 'en attente';
            });

        this.storage.get('carts').then((data) => {
            if (data) {
                this.panierLength = data.length;
            }
        });

    }

    cart() {
        this.navCtrl.push('CartPage')
    }

    refuser() {
        this.commandeClientsService.refuseCommande(this.usersApi.getUser().token, this.food.cmd.id)
            .subscribe((data: any) => {
                let toast = this.toastCtrl.create({
                    message: "Votre commande a été annulée",
                    duration: 1000,
                    position: 'buttom'
                });
                toast.present();
            });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    roNumber(x) {
        return x;
    }

    public getSafehtml(text) {
        var map = {
            '&amp;': '&',
            '&#038;': "&",
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'",
            '&#8217;': "’",
            '&#8216;': "‘",
            '&#8211;': "–",
            '&#8212;': "—",
            '&#8230;': "…",
            '&#8221;': '”'
        };

        return text.replace(/\&[\w\d\#]{2,5}\;/g, function (m) {
            return map[m];
        });


    }

    getInfo() {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
            /* spinner: 'hide',
                  content: '  <div class="custom-spinner-box"> <img src="assets/images/chargemen11t.gif"></img> </div>' */
        });
        //this.loading.present();

        this.storesApi.getInfoStripeCard(this.usersApi.getUser().token).subscribe(
            data => this.wsSuccessCardGet(data),
            error => this.wsFailure(<any>error));
    }

    wsSuccessCardGet(data) {
        this.loading.dismissAll();
        if (data.card != null) {
            this.myCardData = data.card;

        }

    }

    private wsFailure(err) {
        //
        this.loading.dismissAll();
        let actionSheet = this.alertCtrl.create({
            title: 'erreur connexion serveur',
            buttons: [
                {
                    text: 'ok',
                    role: "cancel"
                }
            ],
            enableBackdropDismiss: false
        });
        actionSheet.present();
    }

}
