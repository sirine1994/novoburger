import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController, Slides} from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {CallNumber} from '@ionic-native/call-number';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {TranslateService} from "@ngx-translate/core";
import {RestGlobals} from "../../providers/rest-globals";

@IonicPage()
@Component({
    selector: 'page-searchStore',
    templateUrl: 'searchStore.html'
})
export class searchStorePage {
    @ViewChild("mySlider") slides: Slides;
    list: any;
    sliderLength: any;
    listStore: any;
    base_url: any;
    total_card: any = 0;
    settings: any = '';
    slider: any;
    loading: any;
    isSerch: boolean = false;
    previousLoading: any;
    myIndex: number = 0;
    categoriId: string;
    adress: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: Http,
        public callNumber: CallNumber,
        public loadingCtrl: LoadingController,
        public translate: TranslateService,
        public iab: InAppBrowser,
        public toastCtrl: ToastController,
        public storage: Storage) {
        this.base_url = Constant.domainConfig.base_url;
        this.previousLoading = this.navParams.get("loading");
        this.searchSubmit();

        this.storage.ready().then(() => {
            this.storage.get('settings').then(data => {
                this.settings = data;
            })
        })
    }


    cart() {
        this.navCtrl.push('CartPage')
    }

    ionViewWillEnter() {
        this.storage.get('carts').then((obj) => {
            if (obj) this.total_card = obj.length;
            if (this.previousLoading) {
                this.previousLoading.dismiss();
            }
        });

    }

    public getSafehtml(text) {
        var map = {
            '&amp;': '&',
            '&#038;': "&",
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'",
            '&#8217;': "’",
            '&#8216;': "‘",
            '&#8211;': "–",
            '&#8212;': "—",
            '&#8230;': "…",
            '&#8221;': '”'
        };
        return text.replace(/\&[\w\d\#]{2,5}\;/g, function (m) {
            return map[m];
        });


    }

    onSelectChange(selectedValue: any) {
        let index = this.myIndex;
    }


    searchSubmit() {
        this.translate.get(['loading_list_resto', 'no_product']).subscribe(res => {

            this.loading = this.loadingCtrl.create({
                content: res.loading_list_resto
            });
            this.loading.present();
        });
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        this.http
            .get(Constant.domainConfig.base_url + "api/store_api/list", {
                headers: headers
            })
            .subscribe(
                (data: any) => {
                    this.listStore = data.json().store;
                    this.loading.dismiss();
                    this.getSlider();
                },
                error => {
                    this.loading.dismiss();
                }
            );


    }

    getSlider() {
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        let i = 0;

        for (i = 0; i < this.listStore.length; i++) {
            let idstore = this.listStore[i].id;
            // alert(JSON.stringify(idstore));
            this.http
                .get(Constant.domainConfig.base_url + "api/slider_api/get?store_id=" + idstore, {
                    headers: headers
                })
                .subscribe(
                    (data: any) => {
                        this.slider = data.json().slider;
                       this.sliderLength=this.slider.length;
                        this.loading.dismiss();
                    },
                    error => {
                        this.loading.dismiss();
                    }
                );
        }

    }

    openPage(item) {
        RestGlobals.idStore = item.id;
        RestGlobals.stripeAccount = item.stripeAccount;
        RestGlobals.storeName = item.name;
        RestGlobals.discount="";
        this.storage.remove("carts");
           RestGlobals.data=null;
        this.navCtrl.push('CategoriesPage', {id: item.id, name: item.name});
    }

    openCartPage() {
        this.translate.get(['loading_data']).subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.loading_data
            });
        });
        this.loading.present();
        this.navCtrl.setRoot('CartPage', {loading: this.loading});
    }

    search() {
        this.isSerch = !this.isSerch;
    }


    facebook() {
        let browser = this.iab.create(this.settings.facebook, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    twitter() {
        let browser = this.iab.create(this.settings.twitter, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    call() {
        this.callNumber.callNumber(this.settings.phone, true);
    }

}
