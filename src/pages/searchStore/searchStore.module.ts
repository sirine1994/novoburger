import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { searchStorePage } from './searchStore';
import { TranslateModule } from '@ngx-translate/core';
import {InAppBrowser} from "@ionic-native/in-app-browser";

@NgModule({
  declarations: [
      searchStorePage
  ],
  imports: [
    IonicPageModule.forChild(searchStorePage),
    TranslateModule.forChild()
  ],
    providers: [InAppBrowser],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CsearchStoreModule {

}
