import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the ListCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-card',
  templateUrl: 'list-card.html',
})
export class ListCardPage {
    loading: any;
    panierLength:number=0;
    myCardData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public storesApi: UserProvider,public storage : Storage, public usersApi: UserProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListCardPage');
      this.storage.get('carts').then((data) => {
          if(data){this.panierLength=data.length;
          }
      });
  }
    addCard(){
        this.navCtrl.push('StoreCardPage')
    }
    editCard(){

    }
    cart(){
        this.navCtrl.push('CartPage')
    }
    getInfo(){
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
            /* spinner: 'hide',
                  content: '  <div class="custom-spinner-box"> <img src="assets/images/chargemen11t.gif"></img> </div>' */
        });
        //this.loading.present();

        this.storesApi.getInfoStripeCard(this.usersApi.getUser().token).subscribe(
            data => this.wsSuccessCardGet(data),
            error => this.wsFailure(<any>error)) ;
    }

    wsSuccessCardGet(data){
        this.loading.dismissAll();
        if(data.card!=null){
            this.myCardData = data.card;

        }

    }

    private wsFailure(err){
        //
        this.loading.dismissAll();
        let actionSheet = this.alertCtrl.create({
            title: 'erreur connexion serveur',
            buttons: [
                {
                    text: 'ok',
                    role: "cancel"
                }
            ],
            enableBackdropDismiss: false
        });
        actionSheet.present();
    }
}
