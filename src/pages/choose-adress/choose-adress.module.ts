import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChooseAdressPage } from './choose-adress';

@NgModule({
  declarations: [
    ChooseAdressPage,
  ],
  imports: [
    IonicPageModule.forChild(ChooseAdressPage),
  ],
})
export class ChooseAdressPageModule {}
