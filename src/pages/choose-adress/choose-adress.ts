import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import * as Constant from "../../config/constants";
import {Http} from "@angular/http";
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the ChooseAdressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-choose-adress',
    templateUrl: 'choose-adress.html',
})
export class ChooseAdressPage {

    list: any;

    constructor(public navCtrl: NavController,
                public http: Http,
                public navParams: NavParams,
                public userService: UserProvider,
                public modalCtrl: ModalController,
                public viewCtrl: ViewController) {
    }

    ionViewDidLoad() {
        this.loadMore();
    }

    loadMore() {
        this.http.get(Constant.domainConfig.base_url + 'api/adresse_api/get?token=' + this.userService.getUser().token).subscribe(data => {
            this.list = data.json().mes_adresses;


        });
    }

    chooseAdrr(data) {
        this.viewCtrl.dismiss(data);

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}
