import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import {UserProvider} from '../../providers/user/user';
import {tokenKey} from '@angular/core/src/view';
import {BoCommandesProvider} from '../../providers/bo-commandes/bo-commandes';
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the HistoriqueCommandesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-historique-commandes',
    templateUrl: 'historique-commandes.html',
})
export class HistoriqueCommandesPage {
    commandesList: any[];
    first = 0;
    public date_picked = {
        month: new Date().toISOString().split('T')[0],
        timeStarts: '07:43',
        timeEnds: '1990-02-20'
    };
    token: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public userService: UserProvider,
                public translate: TranslateService,
                public commandeBoService: BoCommandesProvider,
                public loadingCtrl: LoadingController,
                private alertCtrl: AlertController,) {
    }

    ionViewDidLoad() {
        //
        this.token = this.userService.getUser().token;

        this.loadData();
    }

    cart(){
        this.navCtrl.push('CartPage')
    }

    private loadData() {
        this.translate.get(['loadingC', 'Operation_not_performed']).subscribe(res => {

            let loader = this.loadingCtrl.create({
                content: res.loadingC
            });
            loader.present();
            this.commandeBoService.getHistoriqueBo(this.token, this.date_picked.month, this.first)
                .subscribe((data: any) => {

                        this.commandesList = data.data;
                        loader.dismiss();
                    },
                    err => {
                        loader.dismiss();
                        let alert = this.alertCtrl.create({
                            'message': res.Operation_not_performed
                        });
                        alert.present();
                    });
        });
    }


    loadMore(infiniteScroll: any = null) {
        this.first += 1;


        this.commandeBoService.getHistoriqueBo(this.token, this.date_picked.month, this.first)
            .subscribe((orderCommand: any) => {
                    // //
                    // //
                    orderCommand.data.forEach(x => {
                        this.commandesList.push(x);
                    });
                    if (infiniteScroll) {
                        infiniteScroll.complete();
                    }
                },
                error => {
                    if (infiniteScroll != null) {
                        infiniteScroll.enable(false);
                    }
                });

    }


    dateChanged(event) {

        this.loadData();
    }
}
