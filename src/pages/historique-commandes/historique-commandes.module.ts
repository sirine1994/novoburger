import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoriqueCommandesPage } from './historique-commandes';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    HistoriqueCommandesPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoriqueCommandesPage),
      TranslateModule.forChild()
  ],
})
export class HistoriqueCommandesPageModule {}
