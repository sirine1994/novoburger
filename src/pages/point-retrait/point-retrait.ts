import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import * as Constant from "../../config/constants";
import {Headers, Http} from "@angular/http";
import {UserProvider} from "../../providers/user/user";
import {CallNumber} from "@ionic-native/call-number";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {SocialSharing} from "@ionic-native/social-sharing";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the PointRetraitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-point-retrait',
    templateUrl: 'point-retrait.html',
})
export class PointRetraitPage {
    myZone: any;
    myOrderDate: string;
    data: any;
    settings:any='';

    constructor(public userService: UserProvider, public http: Http, public navCtrl: NavController, public navParams: NavParams,
                public callNumber: CallNumber, public iab: InAppBrowser, public socialSharing: SocialSharing,    public storage: Storage) {
        this.myZone = new Array();
        this.myOrderDate = this.navParams.get("dateOrder");
        this.data = new Array();
        this.storage.ready().then(() => {
            this.storage.get('settings').then(obj=>{
                this.settings=obj;
            });
        }) }

    ionViewDidLoad() {
        this.data = this.navParams.get("data");
        console.log('ionViewDidLoad PointRetraitPage');
        this.loadPoint();
    }

    loadPoint() {
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        this.http
            .post(Constant.domainConfig.base_url + "api/point_api/show?token=" + this.userService.getUser().token, {date: this.myOrderDate}, {
                headers: headers
            })
            .subscribe(
                (data: any) => {
                    this.wsSuccessPoint(data.json());
                },
                error => {

                }
            );
    }

    onChangePoint(id) {

        this.data.point = id;
    }

    goBack() {
        this.navCtrl.push('CheckoutPage', {"data": this.data, "livraison": 1});

    }

    wsSuccessPoint(data) {
        this.myZone = data.object.zone;
        console.log(JSON.stringify(this.myZone));
    }
    share(item){
        this.socialSharing.share(item.name, item.description, null , Constant.domainConfig.base_url+'food?id='+item.id);
    }

    facebook(){
        let  browser = this.iab.create(this.settings.facebook, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    twitter(){
        let  browser = this.iab.create(this.settings.twitter, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    call(){
        this.callNumber.callNumber(this.settings.phone,true);
    }
}
