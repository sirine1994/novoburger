import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PointRetraitPage } from './point-retrait';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    PointRetraitPage,
  ],
  imports: [
    IonicPageModule.forChild(PointRetraitPage),
      TranslateModule.forChild()
  ],
})
export class PointRetraitPageModule {}
