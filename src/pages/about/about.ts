import {UserProvider} from './../../providers/user/user';
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {AlertController} from 'ionic-angular';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {CallNumber} from '@ionic-native/call-number';
import {TranslateService} from "@ngx-translate/core";

@IonicPage()
@Component({
    selector: 'page-about',
    templateUrl: 'about.html'
})

export class AboutPage {
    obj: any = '';
    user: any;
    phone:string;
    private form_contact: FormGroup;
idcmd:string;
    constructor(public navCtrl: NavController,
                public iab: InAppBrowser,
                public translate: TranslateService,
                public navParams: NavParams,
                public http: Http,
                public callNumber: CallNumber,
                private alertCtrl: AlertController,
                private formBuilder: FormBuilder,
                public storage: Storage) {
this.idcmd=this.navParams.get('idcmd');
        this.http.get(Constant.domainConfig.base_url + 'api/settings_api/settings').subscribe(data => {
            this.obj = data.json();
            // //
        });
        this.form_contact = this.formBuilder.group({
            full_name: ["", Validators.compose([Validators.minLength(5), Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: ["", Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            message: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(200), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        });
        let that = this;
        this.storage.get("user").then(obj => {
this.phone=obj.phone;

            that.form_contact.controls["full_name"].setValue(obj.full_name);
            that.form_contact.controls["email"].setValue(obj.email);
        });

    }

    nav_more(url) {
        let browser = this.iab.create(url);
    }

    send_message() {
   

        let full_name = this.form_contact.value.full_name;
        let email = this.form_contact.value.email;
        let message = this.form_contact.value.message;
let obj={full_name:full_name,email:email,message:'cmdID:'+this.idcmd+'==>'+message,phone:this.phone};
        let headers: Headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        
        if (full_name != null && email != null && message != null && full_name != '' && email != '' && message != '') {
            this.http.post(Constant.domainConfig.base_url + 'api/contact_api/contact', obj, {headers: headers}).subscribe(data => {
                if (data.json() != null) {
                    this.translate.get(['success_msg', 'ok']).subscribe(res => {
                        let alert = this.alertCtrl.create({
                            'message': res.success_msg,
                            buttons: [{
                                text: res.ok,
                                role: 'cancel',
                                handler: data => {
                                }
                            }]
                        });
                        alert.present();
                    });
                }
            });
        }
    }

    facebook() {
        let browser = this.iab.create(this.obj.facebook);
    }

    twitter() {
        let browser = this.iab.create(this.obj.twitter);
    }

    call() {
        this.callNumber.callNumber(this.obj.phone, true);
    }

    goBack(){ 
    this.navCtrl.pop();
    }

}
