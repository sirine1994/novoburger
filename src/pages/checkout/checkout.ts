import {Component, ElementRef, NgZone, ViewChild} from "@angular/core";
import {
    IonicPage,
    NavController,
    NavParams,
    ToastController,
    LoadingController,
    Loading, ViewController, ModalController
} from "ionic-angular";
import {Http, Headers} from "@angular/http";
import * as Constant from "../../config/constants";
import {Storage} from "@ionic/storage";
import {AlertController} from "ionic-angular";

import {NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult} from "@ionic-native/native-geocoder";

import {Events} from "ionic-angular";
import {CurrencyProvider} from "../../providers/currency";

import {CallNumber} from "@ionic-native/call-number";
import {InAppBrowser} from "@ionic-native/in-app-browser";


import {AdministrationProvider} from "../../providers/administration/administration";

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BoCommandesProvider} from "../../providers/bo-commandes/bo-commandes";
import {RestGlobals} from "../../providers/rest-globals";
import {CartPage} from "../cart/cart";
import {TranslateService} from "@ngx-translate/core";
import {UserProvider} from "../../providers/user/user";
import {ChooseAdressPage} from "../choose-adress/choose-adress";

declare var google;
declare var stripe: any;

@IonicPage()
@Component({
    selector: "page-checkout",
    templateUrl: "checkout.html"
})
export class CheckoutPage {
    myZone: any;
    markers: any;
    autocomplete: any;
    GoogleAutocomplete: any;
    GooglePlaces: any;
    autocompleteItems: any;
    SearchForm: FormGroup;
    actualDateTime = new Date().toISOString();

    actualDate: any;
    actualTime: any;

    token: string;
    IsTimeValid: boolean = false;
    IsEmporter: boolean;
    IsLivraison: boolean;
    IsStuart: boolean;
    IsOnLignePaiement = false;
    IsCachPaiement = false;

    AfternoonFrom: string;
    AfternoonTo: string;
    isValidAdress: boolean;
    MorningFrom: string;
    MorningTo: string;
    loader: Loading;
    IsOuvert = false;
    ThereIsService = false;
    messageStipeError: string = "";
    minimumPrice: number;
    maximumDistance: number;

    total: any;
    full_name: string = "";
    phone: string = "";
    address: string = "";
    message: string = "";
    check: string = "";
    pay_method: number;
    delivary_type: string;
    zone_couverture: number;
    email: string = "";
    errorMessage: string = "";
    data: any;
    user_id: string = "";
    livraisonObject: any;
    loading: any;

    title: string = "My first AGM project";
    lat: number = 44.1212459;
    lng: number = 2.916215;

    Paiement_method: string;

    distance_chalendise = 0;
    isValidDistance = false;

    /*this card info just for test, pls set it to empty string when your app go online */
    card_info: any = {
        number: "4242424242424242",
        expMonth: 12,
        expYear: 2020,
        cvc: "220"
    };
    idpormo: string;
    isDelivaryNow = true;
    total_card: any = 0;
    carts: any = "";
    complementAdresse: string;
    public zoom: number;
    settings: any = "";
    validate: boolean = true;
    validatePrice: boolean = true;
    DelivaryDateNow: Date;
    DelivaryDate: string = null;
    DelivaryTime: string;
    islivraisonActivated: number;

    constructor(private navCtrl: NavController,
                public navParams: NavParams,
                public viewCtrl: ViewController,
                public userService: UserProvider,
                public http: Http,
                public storage: Storage,
                public translate: TranslateService,
                public formBuilder: FormBuilder,
                public callNumber: CallNumber,
                public iab: InAppBrowser,
                public toastCtrl: ToastController,
                public geocoder: NativeGeocoder,
                public events: Events,
                public currencyProvider: CurrencyProvider,
                public loadingCtrl: LoadingController,
                private alertCtrl: AlertController,
                private administrationService: AdministrationProvider,
                public modalCtrl: ModalController,
                private ngZone: NgZone,
                private boCommandeService: BoCommandesProvider) {
        this.livraisonObject = {frais_livraison: 0, frais_petit_commande: 0, frais_service: 0};
        // this.pay_method = 0;
        this.storage.get("user").then(obj => {
            if (obj == null) {
                this.user_id = null;
            } else {

                this.user_id = obj.id;
                this.full_name = obj.full_name;
                this.phone = obj.phone;
                this.email = obj.email;
                //this.TimeSchedule();
            }
        });

        this.SearchForm = formBuilder.group({

            codePostal: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(5), Validators.pattern('[A-z0-9À-ž\s ]*'), Validators.required])],

        });
        events.subscribe("user:add_cart", (user, time) => {
            this.total_card += 1;
        });

        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.autocomplete = {
            input: ''
        };
        this.autocompleteItems = [];
        this.markers = [];
        this.storage.ready().then(() => {
            this.storage.get("carts").then(data => {
                this.carts = data;

            });

            this.storage.get("user").then(data => {
                this.token = data.token;
                // // //
                this.orderIsValid();
            });

            this.storage.get("settings").then(data => {
                this.settings = data;
            });
            // this.getUserData();
        });

        this.events.subscribe("user: change", () => {
            this.ionViewDidLoad();
        });


        //set current position
        // this.setCurrentPosition();

        this.zoom = 4;

        this.GetDateTimeNow();


    }

    private GetDateTimeNow() {
        this.DelivaryDateNow = new Date();
        this.DelivaryDate = this.DelivaryDateNow.getFullYear().toString() + '-' + this.DelivaryDateNow.getMonth().toString() + '-' + this.DelivaryDateNow.getDay().toString();
        this.DelivaryTime = this.DelivaryDateNow.getHours().toString() + ':' + this.DelivaryDateNow.getMinutes().toString();
    }

    goBack() {
        this.viewCtrl.dismiss();
    }

    getUserData() {
        this.translate.get(['user_verification']).subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.user_verification
            });
        });
        this.loading.present();

        let userHaveCard = false;
        return this.http.get(
            Constant.domainConfig.base_url +
            "api/users_api/one_user?token=" +
            this.token
        );
    }

    dismiss()
    {
        this.viewCtrl.dismiss();
    }
    IsCouvert(event) {
        if (10 < 5) {
            return true;
        } else {
            this.translate.get(['zone_not_available', 'ok']).subscribe(res => {

                let alert = this.alertCtrl.create({
                    message:
                    res.zone_not_available,
                    buttons: [
                        {
                            text: res.ok,
                            role: "cancel",
                            handler: data => {
                                this.delivary_type = "livraison";
                            }
                        }
                    ]
                });
                // loader.dismiss();
                alert.present();

                return false;
            });
        }
    }


    ionViewDidLoad() {
    
        this.idpormo = this.navParams.get("idpormo");
        this.islivraisonActivated = this.navParams.get("livraison");
         this.total = this.navParams.get("total");
        this.orderIsValid();
        RestGlobals.data = this.navParams.get("data");
        if (RestGlobals.data && RestGlobals.data.livraisonObject)
            this.livraisonObject = RestGlobals.data.livraisonObject;
        var x = (new Date()).getTimezoneOffset() * 60000;
        var localISOTime = (new Date(Date.now() - x));

        this.actualDateTime = (new Date(localISOTime.getTime() + 30 * 60000)).toISOString().slice(0, -1);

        this.actualDate = (this.actualDateTime).split("T")[0];
        this.actualTime = this.actualDateTime.split("T")[1];

        this.storage.get("carts").then(obj => {
            if (obj) this.total_card = obj.length;
        });
        // if(this.islivraisonActivated==1) this.delivary_type="livraison";
        //this.IsEmporter=this.navParams.get("emporter");

        this.pay_method == 0;

        console.log("eeeeeeeeee " + JSON.stringify(RestGlobals.data));
        if (RestGlobals.data == null) {

            this.DelivaryDate = this.actualDate;

            RestGlobals.data = {
                'email': this.email,
                'total': this.total,
                'note': "",
                'store_id': RestGlobals.idStore,
                'full_name': this.full_name,
                'user_id': this.user_id,
                'phone': this.phone,
                'address': this.address,
                'lat': 44.1212459,
                'lng': 1.916215,
                'livraisonObject': this.livraisonObject,
                'complement': this.complementAdresse,
                'type': this.delivary_type,
                'type_Paiement': this.Paiement_method,
                'delivery_date': this.actualDate + "T" + this.actualTime,
                'message': this.message,
                'items': this.carts,
                'payment_method': "test",
                'code': this.idpormo,
                'point': ''
            };
            this.delivary_type = "emporter";
        } else {

            this.Paiement_method = RestGlobals.data.type_Paiement;
            this.address = RestGlobals.data.address;
            this.autocomplete = {
                input: this.address
            };
            let myDat = new Date().toISOString();

            if (RestGlobals.data.delivery_date) {
                myDat = new Date(RestGlobals.data.delivery_date).toISOString();

                this.DelivaryDate = myDat.split("T")[0];
                this.actualTime = RestGlobals.data.delivery_date.split(" ")[1];

            }
            if (this.Paiement_method == "livraison") {
                this.pay_method = 0;
            } else
                this.pay_method = 1;
            if (RestGlobals.data.type != "")
                this.delivary_type = RestGlobals.data.type;
            else
                this.delivary_type = "emporter";

            //this.delivary_type="emporter";

        }

        this.storage.get("user").then(obj => {
            if (obj == null) {
                this.user_id = null;
            } else {

                this.user_id = obj.id;
                this.full_name = obj.full_name;
                this.phone = obj.phone;
                this.email = obj.email;
                //this.TimeSchedule();
            }
        });


        /* this.actualTime =
           this.actualDateTime.split("T")[1].split(":")[0];*/

        /* if(this.islivraison==1 && this.delivary_type=='livraison'){
             this.mapsAPILoader.load().then(() => {
                  let nativeHomeInputBox =document.getElementById('txtAdresse2')
                     .getElementsByTagName('input')[0];
                 let autocomplete = new google.maps.places.Autocomplete(
                     nativeHomeInputBox,
                     {
                         types: ["address"],
                         componentRestrictions:{country:"fr"}
                     }
                 );
                 let actualAdresse: string;
                 autocomplete.addListener("place_changed", () => {
                     this.ngZone.run(() => {
                         //get the place result
                         let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                         //verify result
                         this.address = place.formatted_address;
                         if (place.geometry === undefined || place.geometry === null) {
                             return;
                         }

                         //set latitude, longitude and zoom
                         this.lat = place.geometry.location.lat();
                         this.lng = place.geometry.location.lng();
                         this.zoom = 12;

                       //  this.calculateDistance(this.lat,this.lng);
                     });
                 });
             });}*/
            
             if(RestGlobals.data.type==""||typeof(RestGlobals.data.type)=="undefined")
                 RestGlobals.data.type="emporter";
             
setTimeout(() => {this.changedDelivry();},1800);
    }

    updateSearchResults() {
        let config = {
            types: ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: this.autocomplete.input,
            componentRestrictions: {country: 'FR'}
        };
        if (this.autocomplete.input == '') {

            this.autocompleteItems = [];
            return;
        }

        this.GoogleAutocomplete.getPlacePredictions(config,
            (predictions, status) => {
                this.autocompleteItems = [];
                if (predictions) {
                    //this.zone.run(() => {
                    predictions.forEach((prediction) => {

                        this.autocompleteItems.push(prediction);
                    });
                    // });
                }
            });
    }

    selectSearchResult(item) {


        this.autocompleteItems = [];


        // this.SearchForm.controls.codePostal.setValue(item.description);


        this.autocomplete.input = item.description;
        this.address = item.description;
        this.lat = 43.5959281;
        this.lng = 1.4233221;
        //this.calculateDistance(this.lat, this.lng);
        this.updateStuart();
        this.geocoder.forwardGeocode(item.description)
            .then((coordinates: NativeGeocoderForwardResult[]) => {

                    this.lat = Number(coordinates[0].latitude);

                    this.lng = Number(coordinates[0].longitude);
                    this.updateStuart();
                    //this.calculateDistance(this.lat, this.lng);


                }
            )
            .catch((error: any) => console.log(error)
            );


    }

    changedDelivry() { 

    if(this.delivary_type)
        RestGlobals.data.type = this.delivary_type;

        this.TimeSchedule(RestGlobals.data.type);
        var x = (new Date()).getTimezoneOffset() * 60000;

        if (RestGlobals.data.type == "stuart") {

            var localISOTime = (new Date(Date.now() - x));
            this.actualDateTime = (new Date(localISOTime.getTime() + 5 * 60000)).toISOString().slice(0, -1);
            this.actualDate = (this.actualDateTime).split("T")[0];
            this.actualTime = this.actualDateTime.split("T")[1];

        } else {

            var localISOTimee = (new Date(Date.now() - x));
            this.actualDateTime = (new Date(localISOTimee.getTime() + 30 * 60000)).toISOString().slice(0, -1);
            this.actualDate = (this.actualDateTime).split("T")[0];
            this.actualTime = this.actualDateTime.split("T")[1];

        }
        RestGlobals.data.type = this.delivary_type;
        this.updateStuart();


    }

    // getAdresse() {
    //   this.calculateDistance(this.lat,this.lng);
    // }
    /* order_now() {


       //this.stripe.setPublishableKey(Constant.stripe_publish_key);

       this.getUserData().subscribe(data => {
         this.loading.dismiss();

         if (data.json()[0].hasCard == "1") {
           let userdata =
             "email=" +
             this.email +
             "&total=" +
             this.total +
             "&full_name=" +
             this.full_name +
             "&phone=" +
             this.phone +
             "&user_id=" +
             this.user_id +
             "&address=" +
             this.address +
             "&complement=" +
             this.complementAdresse +
             "&type=" +
             this.delivary_type +
             "&type_Paiement=" +
             this.Paiement_method +
             "&delivery_date=" +
             this.DelivaryDate +
             "&delivery_time=" +
             this.DelivaryTime +
             "&message=" +
             this.message +
             "&items=" +
             JSON.stringify(this.carts);
           this.post(userdata);
         } else {
           stripe
             .createCardToken(this.card_info)
             .then(token => {
               let headers: Headers = new Headers({
                 "Content-Type": "application/x-www-form-urlencoded"
               });

               var data_stripe = "token=" + this.token + "&token_card=" + token.id;
               this.http
                 .post(
                   Constant.domainConfig.base_url + "api/stripe_api/create_card",
                   data_stripe,
                   { headers: headers }
                 )
                 .subscribe(
                   data => {
                     let userdata =
                       "email=" +
                       this.email +
                       "&total=" +
                       this.total +
                       "&token=" +
                       token["id"] +
                       "&full_name=" +
                       this.full_name +
                       "&phone=" +
                       this.phone +
                       "&user_id=" +
                       this.user_id +
                       "&address=" +
                       this.address +
                       "&type=" +
                       this.delivary_type +
                       "&type_Paiement=" +
                       this.Paiement_method +
                       "&delivery_date=" +
                       this.DelivaryDate +
                       "&delivery_time=" +
                       this.DelivaryTime +
                       "&message=" +
                       this.message +
                       "&items=" +
                       JSON.stringify(this.carts);
                     this.post(userdata);
                   },
                   error => {
                     // //
                     // //
                   }
                 );
             })
             .catch(error => {
               let alert = this.alertCtrl.create({
                 message: error
               });
               alert.present();
             });
         }
       });
     }*/

    /* order_on_paypal() {
         if (this.user_id == null) {
             if (this.full_name.length < 5 || this.full_name.length > 50) {
                 this.validate = false;
                 return;
             }

             if (this.phone.length < 10 || this.phone.length > 50) {
                 this.validate = false;
                 return;
             }
         }

         if (this.address.length < 5) {
             this.validate = false;
             return;
         }


         if (this.user_id == null) {
             if (this.email.length < 5 || this.email.length > 50) {
                 this.validate = false;
                 return;
             }
         }
         if (this.ValidatePrice() == false) {
             return;
         }

         this.validate = true;
         if (this.validate == true) {
             this.payPal
                 .init({
                     PayPalEnvironmentProduction: Constant.paypal_live_client_id,
                     PayPalEnvironmentSandbox: Constant.paypal_sandbox_client_id
                 })
                 .then(
                     () => {
                         this.payPal
                             .prepareToRender(
                                 "PayPalEnvironmentSandbox",
                                 new PayPalConfiguration({})
                             )
                             .then(
                                 () => {
                                     let payment = new PayPalPayment(
                                         this.total,
                                         this.settings.currency_code,
                                         "Buy Pizza",
                                         "sale"
                                     );
                                     this.payPal.renderSinglePaymentUI(payment).then(
                                         () => {
                                             let data =
                                                 "email=" +
                                                 this.email +
                                                 "&total=" +
                                                 this.total +
                                                 "&full_name=" +
                                                 this.full_name +
                                                 "&user_id=" +
                                                 this.user_id +
                                                 "&phone=" +
                                                 this.phone +
                                                 "&type_Paiement=" +
                                                 this.Paiement_method +
                                                 "&address=" +
                                                 this.address +
                                                 "&message=" +
                                                 this.message +
                                                 "&type=" +
                                                 this.delivary_type +
                                                 "&items=" +
                                                 JSON.stringify(this.carts);

                                             this.post(data);
                                         },
                                         () => {
                                         }
                                     );
                                 },
                                 () => {
                                 }
                             );
                     },
                     () => {
                     }
                 );
         }
     }*/

    order_on_delivery() {

       // this.navCtrl.push("CartPage", {myData: RestGlobals.data});
        this.viewCtrl.dismiss(RestGlobals.data);
        //this.post(RestGlobals.data);
    }

    post(data) {
        this.translate.get(['loadingC']).subscribe(res => {
            this.loader = this.loadingCtrl.create({
                content: res.loadingC
            });
        });
        this.loader.present();
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });


        this.http
            .post(Constant.domainConfig.base_url + "api/orders_api/add_order", JSON.stringify(data), {
                headers: headers
            })
            .subscribe(
                (data: any) => {
                    this.wsSuccess(data.json());
                },
                error => {
                    this.loader.dismiss();
                }
            );
    }


    handleAction(response) {
        let that = this;
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });

        stripe.handleCardAction(
            response.payment_intent_client_secret
        ).then(function (result) {
            if (result.error) {
                this.translate.get(['ok', 'err_pay']).subscribe(res => {

                    let actionSheet = that.alertCtrl.create({
                        title: res.err_pay,
                        buttons: [
                            {
                                text: res.ok,
                                role: 'cancel'
                            }
                        ], enableBackdropDismiss: false
                    });
                    actionSheet.present();
                });
            } else {
                // The card action has been handled
                // The PaymentIntent can be confirmed again on the server
                that.data.payment_method = result.paymentIntent.id;
                that.http
                    .post(Constant.domainConfig.base_url + "api/orders_api/add_order", that.data, {
                        headers: headers
                    })
                    .subscribe(
                        (data: any) => {
                            that.wsSuccess(data.json());
                        },
                        error => {
                            that.loader.dismiss();
                        }
                    );

            }
        });
    }

    wsSuccess(data) {


        if (data) {

            this.messageStipeError = null;
            console.log((data.requires_action));
            if (data.requires_action == null) {//||data.payment_stripe_return.success
                this.translate.get(['err_ordering', 'success_ordering', 'ok']).subscribe(res => {

                    console.log(data);
                    if (data.ok == false) {
                        let alert = this.alertCtrl.create({
                            message: res.err_ordering
                        });
                        alert.present();
                        if (this.loader) this.loader.dismiss();
                    } else {
                        this.storage.set("carts", new Array());
                        let alert = this.alertCtrl.create({
                            message:
                            res.success_ordering,
                            buttons: [
                                {
                                    text: res.ok,
                                    role: "cancel",
                                    handler: data => {
                                        //this.navCtrl.pop();
                                        this.navCtrl.setRoot("CategoriesPage");
                                    }
                                }
                            ]
                        });
                        if (this.loader) this.loader.dismiss();
                        alert.present();
                    }

                });
            } else if (!data.requires_action) {
                this.errorMessage = data.stripe_message_error;
            } else if (data.requires_action == true) {
                // Use Stripe.js to handle required card action
                // if(data.automatic)
                this.handleAction(data);
                /*else if((this.counter+1)<Shared.panierList.length)
                {this.counter++;
                    this.commandeApi.setCommande(RestGlobals.token, [Shared.panierList[(this.counter)]], "test")
                        .subscribe(
                            data => this.wsSuccess(data),
                            error => this.wsFailure(<any>error));
                }*/

            } else {
                this.handleAction(data);
            }
        }

    }

    couldMakeOrder() {

        let validationPrice = false;

        if (this.ValidatePrice()) {
            validationPrice = true;
        }
        validationPrice = true;


        if (this.MorningFrom !== undefined) {
            this.IsTimeValid = this.InLivraisonTime(this.actualTime);
        }

        if (this.delivary_type == 'livraison') {
            return this.IsTimeValid && validationPrice ;
        } else {
            return this.IsTimeValid && validationPrice;//&&this.isValidDistance
        }
    }


    livraison() {

        // RestGlobals.data.type = "emporter";

        RestGlobals.data.delivery_date = this.DelivaryDate + " " + this.actualTime;

        //this.navCtrl.push('PointRetraitPage', {dateOrder: this.DelivaryDate, data: RestGlobals.data});
        //  this.navCtrl.push('LivraisonPointRetraitPage');

    }

    order(liv) {

        //RestGlobals.data=this.navParams.get("data");

        if (RestGlobals.data == null)
            RestGlobals.data = {
                'email': this.email,
                'total': this.total,
                'note': "",
                'reduction': 0,
                'store_id': RestGlobals.idStore,
                'full_name': this.full_name,
                'user_id': this.user_id,
                'phone': this.phone,
                'address': this.address,
                'lat': 44.1212459,
                'lng': 1.916215,
                'livraisonObject': this.livraisonObject,
                'complement': this.complementAdresse,
                'type': this.delivary_type,
                'type_Paiement': this.Paiement_method,
                'delivery_date': this.DelivaryDate + " " + this.actualTime,
                'message': this.message,
                'items': this.carts,
                'payment_method': "test",
                'code': this.idpormo,
                'point': ''
            };
        else {

            // this.Paiement_method= RestGlobals.data.type_Paiement;
            if (liv == 2) {

                let point = RestGlobals.data.point;
                RestGlobals.data = {
                    'email': this.email,
                    'total': this.total,
                    'note': RestGlobals.data.note,
                    'reduction': 0,
                    'store_id': RestGlobals.idStore,
                    'full_name': this.full_name,
                    'user_id': this.user_id,
                    'phone': this.phone,
                    'address': this.address,
                    'livraisonObject': this.livraisonObject,
                    'lat': 44.1212459,
                    'lng': 1.916215,
                    'complement': this.complementAdresse,
                    'type': this.delivary_type,
                    'type_Paiement': this.Paiement_method,
                    'delivery_date': this.DelivaryDate + " " + this.actualTime,
                    'message': this.message,
                    'items': this.carts,
                    'payment_method': "test",
                    'code': this.idpormo,
                    'point': point
                };

            }
        }

        if (!this.couldMakeOrder() && liv == 2) {
            this.toastMessage("Horaire indisponible ou adresse non valide !");
        } else if (!this.address && (RestGlobals.data.type == "stuart" || RestGlobals.data.type == "livraison") && liv == 2) {
            this.toastMessage("Veuillez insérer une adresse !");
        } else {
            this.order_on_delivery();
        }


        /* setTimeout(() => {

           if (this.pay_method == 0) {
             this.order_on_delivery();
           }
           if (this.pay_method == 1) {
             this.order_now();
           }
         }, 1000);*/
    }

    cancel() {
        this.navCtrl.pop();
    }

    facebook() {
        let browser = this.iab.create(this.settings.facebook, '_blank', {
            location: 'yes',
            toolbar: 'yes',
            closebuttoncaption: 'Fermer'
        });
    }

    twitter() {
        let browser = this.iab.create(this.settings.twitter, '_blank', {
            location: 'yes',
            toolbar: 'yes',
            closebuttoncaption: 'Fermer'
        });
    }

    call() {
        this.callNumber.callNumber(this.settings.phone, true);
    }

    Show_date($event) {
        // // //
    }

    Show_time($event) {
        // // //
    }

    orderIsValid() {
        this.IsEmporter = undefined;
        this.IsLivraison = undefined;
        this.IsStuart = undefined;
        this.IsOnLignePaiement = undefined;
        this.IsCachPaiement = undefined;

        this.administrationService
            .checkZoneChalendise(this.token, RestGlobals.idStore)
            .subscribe((data: any) => {

                try {
                    if (data && data.data.setting_zone.paiement) {
                        switch (data.data.setting_zone.paiement) {
                            case "All": {
                                this.IsOnLignePaiement = true;
                                this.IsCachPaiement = true;
                                // this.pay_method = 0;
                                break;
                            }
                            case "On_line": {
                                this.IsOnLignePaiement = true;
                                this.IsCachPaiement = undefined;
                                //this.pay_method = 1;
                                break;
                            }
                            case "Livraison": {
                                this.IsCachPaiement = true;
                                this.IsOnLignePaiement = undefined;
                                // this.pay_method = 0;
                                break;
                            }
                            default: {
                                //statements;
                                break;
                            }
                        }

                        switch (data.data.setting_zone.type) {
                            case "All": {
                                this.IsEmporter = true;
                                this.IsLivraison = true;
                                this.IsStuart = true;
                                //this.delivary_type = "livraison";
                                break;
                            }


                            default: {
                                //statements;
                                break;
                            }
                        }

                        if (data.data.setting_zone.type.livraison)
                            this.IsLivraison = true;


                        if (data.data.setting_zone.type.emporter)
                            this.IsEmporter = true;


                        if (data.data.setting_zone.type.stuart)
                            this.IsStuart = true;


                        if (this.IsEmporter && !this.IsLivraison)
                            this.livraison();
                        /* if (data.data.setting_zone.hasZone) {
                             this.maximumDistance = data.data.setting_zone.zone;
                         }*/
                    }
                } catch (e) {
                    console.log("error" + e);
                }
            });

        this.administrationService

            .checkMontant(this.token, RestGlobals.idStore)
            .subscribe((data: any) => {
                try {
                    if (data.data.setting_montant.hasMontant) {
                        this.minimumPrice = data.data.setting_montant.montant;
                    }
                } catch (e) {
                    console.log("error" + e);
                }

            });


    }

    changedPaiment() {
        if (this.pay_method == 0) {
            this.Paiement_method = "livraison";
        } else if (this.pay_method == 1) {
            this.Paiement_method = "par_carte";
        }
        //RestGlobals.data=this.navParams.get("data");

        if (RestGlobals.data == null) {

            RestGlobals.data = {
                'email': this.email,
                'total': this.total,
                'note': "",
                'store_id': RestGlobals.idStore,
                'full_name': this.full_name,
                'user_id': this.user_id,
                'phone': this.phone,
                'address': this.address,
                'livraisonObject': this.livraisonObject,
                'lat': 44.1212459,
                'lng': 1.916215,
                'complement': this.complementAdresse,
                'type': this.delivary_type,
                'type_Paiement': this.Paiement_method,
                'delivery_date': this.DelivaryDate,
                'message': this.message,
                'items': this.carts,
                'payment_method': "test",
                'code': this.idpormo,
                'point': ''
            };

        } else {
            RestGlobals.data.type_Paiement = this.Paiement_method;
        }


    }

    goToPaiment() {
        if (RestGlobals.data == null)
            RestGlobals.data = {
                'email': this.email,
                'total': this.total,
                'note': "",
                'store_id': RestGlobals.idStore,
                'full_name': this.full_name,
                'user_id': this.user_id,
                'phone': this.phone,
                'address': this.address,
                'lat': 44.1212459,
                'lng': 1.916215,
                'livraisonObject': this.livraisonObject,
                'complement': this.complementAdresse,
                'type': this.delivary_type,
                'type_Paiement': this.Paiement_method,
                'delivery_date': this.DelivaryDate,
                'message': this.message,
                'items': this.carts,
                'payment_method': "test",
                'code': this.idpormo,
                'point': ''
            };
        RestGlobals.data.Paiement_method = "par_carte";
        let alert = this.alertCtrl.create({
            message:
                'Souhaitez vous modifier ou insérer une carte ?',
            buttons: [
                {
                    text: 'Non',
                    role: "cancel",
                    handler: data => {

                    }
                },
                {
                    text: 'Oui',
                    role: "cancel",
                    handler: data => {
                        this.navCtrl.push('StoreCardPage', {"data": RestGlobals.data});
                    }
                }
            ]
        });
        // loader.dismiss();
        alert.present();

    }

    TimeSchedule(tpe) {



        this.translate.get(['time_check']).subscribe(res => {
            let loader = this.loadingCtrl.create({
                content: res.time_check
            });
            //loader.present();
            this.administrationService
                .checkOpenTime(this.token, this.DelivaryDate, this.lat, this.lng, RestGlobals.idStore)
                .subscribe((data: any) => {
                    try {
                    
                        if (data && data.data.horaire_emporter.length > 0) {
                            if (tpe == "emporter") {
                                this.AfternoonFrom = data.data.horaire_emporter[0].afternoon_from;
                                this.AfternoonTo = data.data.horaire_emporter[0].afternoon_to;
                                this.MorningFrom = data.data.horaire_emporter[0].morning_from;
                                this.MorningTo = data.data.horaire_emporter[0].morning_to;

                            } else if (tpe == "livraison") {
                                this.AfternoonFrom = data.data.horaire_livraison[0].afternoon_from;
                                this.AfternoonTo = data.data.horaire_livraison[0].afternoon_to;
                                this.MorningFrom = data.data.horaire_livraison[0].morning_from;
                                this.MorningTo = data.data.horaire_livraison[0].morning_to;

                            } else {


                                this.AfternoonFrom = data.data.horaire_stuart[0].afternoon_from;
                                this.AfternoonTo = data.data.horaire_stuart[0].afternoon_to;
                                this.MorningFrom = data.data.horaire_stuart[0].morning_from;
                                this.MorningTo = data.data.horaire_stuart[0].morning_to;

                            }
                        }
                    } catch (e) {
                        console.log("morniing" + e);
                    }


                    // loader.dismissAll();
                });
        });

    }


    InLivraisonTime(time: string) {

        let actualTimeNumber = this.getTimeNumber(time);
        if (
            (actualTimeNumber > this.getTimeNumber(this.MorningFrom) && actualTimeNumber < this.getTimeNumber(this.MorningTo)) ||
            (actualTimeNumber > this.getTimeNumber(this.AfternoonFrom) && actualTimeNumber < this.getTimeNumber(this.AfternoonTo))
        ) {
            return true;
        }
        return false;
    }

    getTimeNumber(time: any) {
        // console.log(time);
        let result = +time.split(":")[0] * 60 + +time.split(":")[1];
        return result;
    }

    HasMinimumDistance() {
        switch (this.IsEmporter) {
            case true: {
                return this.maximumDistance;

            }

            default: {
                return false;

            }
        }
    }

    loadAdress() {
       let modal = this.modalCtrl.create("ChooseAdressPage", {});
        modal.present();
        modal.onDidDismiss((data) => {
 if(data){
  this.address = data.adr;
            this.lat = data.lat;
            this.lng = data.lng;
            this.autocomplete.input = data.adr;
            this.complementAdresse = data.complement;
            this.updateStuart();
 }
           
        });

    }


    updateStuart() {
        if ((RestGlobals.data.type == "stuart" || RestGlobals.data.type == "livraison") && this.address) {


            let headers: Headers = new Headers({
                "Content-Type": "application/x-www-form-urlencoded"
            });

            var datastuart = {
                montant: this.total,
                type: RestGlobals.data.type,
                date: this.DelivaryDate,
                time: this.actualTime,
                adresse: this.address,
                token: this.userService.getUser().token,
                store_id: RestGlobals.idStore,
                lat: this.lat,
                lng: this.lng
            };
            this.http
                .post(Constant.domainConfig.base_url + "api/stuart_api/simulate", JSON.stringify(datastuart), {
                    headers: headers
                })
                .subscribe(
                    (data: any) => {
                        this.wsSuccessStuart(data.json());
                    },
                    error => {
                        this.loader.dismiss();
                    }
                );
        } else
            this.livraisonObject = {frais_livraison: 0, frais_petit_commande: 0, frais_service: 0};
    }

    wsSuccessStuart(data) {
        if (data) {
//alert(JSON.stringify(data));

            if (data.has_zone == -1) {
                this.toastMessage("Adresse hors zone de livraison");

            } else {
                this.isValidDistance = true;

                this.livraisonObject = {
                    frais_livraison: data.frais_livraison,
                    frais_petit_commande: data.frais_petit_commande,
                    frais_service: data.frais_service
                };
            }
        }
    }

    ValidatePrice(): boolean {

        if (this.minimumPrice < this.total) {
            this.validatePrice = true;
            return true;
        }
        this.validatePrice = false;
        return false;
    }

    private setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(position => {
                this.lat = position.coords.latitude;

                this.lng = position.coords.longitude;
                this.zoom = 12;
            });
        }
    }

    calculateDistance(lat: number, long: number) {
        this.translate.get(['dist_check']).subscribe(res => {

            let loader = this.loadingCtrl.create({
                content: res.dist_check
            });
        });
        /* this.translate.get(['time_check']).subscribe(res => {

         }*/
        // loader.present();

        this.administrationService
            .checkOpenTime(this.token, this.DelivaryDate, lat, long, RestGlobals.idStore)

            .subscribe((data: any) => {
                //loader.dismiss();

                if (data.data.zone && data.data.zone.length > 0) {
                    this.isValidDistance = true;
                    RestGlobals.data.point = data.data.zone[0].id;
                }
                if (data.data.horaire && data.data.horaire.length > 0) {
                    //  alert(data.data.horaire[0].afternoon_from);
                }
                //     alert(JSON.stringify(data.message));
                this.check = JSON.stringify(data.check);
                this.message = JSON.stringify(data.message);
             
                //this.distance_chalendise = JSON.parse(data._body).distance;
                this.maximumDistance = JSON.parse(data._body).zone;
                // this.isValidDistance = ((Number(this.maximumDistance) - Number(this.distance_chalendise)) >= 0);
            });

    }


    toggleSettingsDelivaryTime(event) {
        // console.log(event);
        this.GetDateTimeNow();
        if (event._value == false) {
            this.DelivaryDate = '';
            // this.DelivaryTime = '';
        }


        this.TimeSchedule(RestGlobals.data.type);
    }


    toastMessage(txt) {
        let toast = this.toastCtrl.create({
            message: txt,
            duration: 1000,
            position: 'buttom'
        });
        toast.present();
    }

}
