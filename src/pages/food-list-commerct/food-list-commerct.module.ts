import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodListCommerctPage } from './food-list-commerct';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    FoodListCommerctPage,
  ],
  imports: [
    IonicPageModule.forChild(FoodListCommerctPage),
    TranslateModule.forChild()
  ],
  exports: [
      FoodListCommerctPage
  ]
})
export class FoodListPageModule {}
