import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  ModalController,
  Events,
  LoadingController
} from "ionic-angular";
import { Http, Headers } from "@angular/http";
import * as Constant from "../../config/constants";
import { Storage } from "@ionic/storage";
import { SocialSharing } from "@ionic-native/social-sharing";
import { CallNumber } from "@ionic-native/call-number";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { CurrencyProvider } from "../../providers/currency";
import { UserProvider } from "../../providers/user/user";
import {FoodDetailCommerctPage} from "../food-detail-commerct/food-detail-commerct";
import {TranslateService} from "@ngx-translate/core";
import {RestGlobals} from "../../providers/rest-globals";

@IonicPage()
@Component({
  selector: "page-food-list-commerct",
  templateUrl: "food-list-commerct.html"
})
export class FoodListCommerctPage {
  base_url: any;
  favoriest_list: Array<any>;
  list: Array<any>;
    listInitial: Array<any>;
    isSerch:boolean=false;
  first: number;
  settings: any = "";
  title: any = "";
  total_card: any=0;
  loading:any;
  constructor(
    public navCtrl: NavController,
    public socialSharing: SocialSharing,
    public navParams: NavParams,
    public translate: TranslateService,
    public http: Http,
    public storage: Storage,
    public callNumber: CallNumber,
    public iab: InAppBrowser,
    public toastCtrl: ToastController,
    public currencyProvider: CurrencyProvider,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    private events: Events,
    public userService: UserProvider
  ) {
    this.base_url = Constant.domainConfig.base_url;
    this.title = this.navParams.get("name");
    this.loading = this.navParams.get("loading");
    events.subscribe("user:add_cart", (user, time) => {
      this.total_card += 1;
    });
  }

  ionViewWillEnter() {
    this.storage.ready().then(() => {
      this.storage.get("settings").then(data => {
        this.settings = data;
      });
      this.storage.get("favoriest_list").then(data => {
        this.favoriest_list = data;
        this.first = 0;
        this.list = new Array();
          this.listInitial = new Array();
        this.loadMore();

        this.loading.dismiss()
      });
      this.storage.get("carts").then(obj => {
       if(obj) this.total_card = obj.length;
      });
    });
  }

    ionClear() {
        this.list = JSON.parse(JSON.stringify(this.listInitial));
    }

    loadMore(infiniteScroll: any = null) {
        /*this.loading = this.loadingCtrl.create({
            content: "Chargement en cours"
        });
        this.loading.present();*/
        this.first += 1;
    this.http.get(
        this.base_url + 'api/foods_api/all_food_menu'+
        '?token=' + this.userService.getUser().token +
        '&categories_id=' + this.navParams.get('id') +
        '&store_id='+this.userService.getUser().store_id+
        '&first=' + this.first
      ).subscribe( (data:any) => {
          var jsonData = JSON.parse(data._body);
          //
        jsonData.food.forEach(element => {
          this.list.push(element);
          this.listInitial.push(element);
        });
//this.loading.dismissAll();

          if (infiniteScroll) {
            infiniteScroll.complete();
          }
        },
        error => {
          if (infiniteScroll != null) {
            infiniteScroll.enable(false);
          }
        }
      );
  }

  addFavoriest(item) {
    if (item.favoriest) {
      item.favoriest = false;
      let index_of = this.favoriest_list.indexOf(item.id);
      this.favoriest_list.splice(index_of, 1);
    } else {
      item.favoriest = true;
      this.favoriest_list.push(item.id);
    }
    this.storage.set("favoriest_list", this.favoriest_list);
  }

  modalAddCart(item) {
    let modal = this.modalCtrl.create("AddCartPage", {
      food_id: item.id,
      discount: item.discount,
      price: item.price
    });
    modal.present();
  }


  modalDatailFood(item){

      this.navCtrl.push("FoodDetailCommerctPage", { id: item.id,type:item.type });
  }

  openPage(id) {
    this.navCtrl.setRoot("FoodDetailCommerctPage", { id: id  });
  }

  openCartPage() {
      this.translate.get(['loading_data']).subscribe(res => {

          this.loading = this.loadingCtrl.create({
              content: res.loading_data
          });
      });
    this.loading.present();
    this.navCtrl.setRoot('CartPage',{loading:this.loading});
  }

    search() {
        this.isSerch = !this.isSerch;
    }

    searchBook(searchbar) {
        // reset countries list with initial call
        this.list = JSON.parse(JSON.stringify(this.listInitial));
        var q = searchbar.target.value;

        // if the value is an empty string don't filter the items
        if (q.trim() == '') {
            return;
        }

        this.list = this.list.filter((v) => {
            console.log("vvv " + JSON.stringify(v));
            //   return (v.toLowerCase().indexOf(q.toLowerCase()) > -1)
            return (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1)
        });

    }

  share(item) {
    this.socialSharing.share(
      item.name,
      item.description,
      null,
      Constant.domainConfig.base_url + "food?id=" + item.id
    );
  }

  facebook() {
    let browser = this.iab.create(this.settings.facebook);
  }

  twitter() {
    let browser = this.iab.create(this.settings.twitter);
  }

  call() {
    this.callNumber.callNumber(this.settings.phone, true);
  }
}
