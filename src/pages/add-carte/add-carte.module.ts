import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCartePage } from './add-carte';

@NgModule({
  declarations: [
    AddCartePage,
  ],
  imports: [
    IonicPageModule.forChild(AddCartePage),
  ],
})
export class AddCartePageModule {}
