import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodDetailCommerctPage} from './food-detail-commerct'
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
      FoodDetailCommerctPage,
  ],
  imports: [
    IonicPageModule.forChild(FoodDetailCommerctPage),
    TranslateModule.forChild()
  ],
  exports: [
      FoodDetailCommerctPage
  ]
})
export class FoodDetailCommerctPageModule {}
