import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ModalController, Events} from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
import {SearchPage} from '../search/search';
import {SocialSharing} from '@ionic-native/social-sharing';
import {CallNumber} from '@ionic-native/call-number';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {CurrencyProvider} from '../../providers/currency';
import {UserProvider} from "../../providers/user/user";
import {TranslateService} from "@ngx-translate/core";

@IonicPage()
@Component({
    selector: 'page-food-detail-commmerct',
    templateUrl: 'food-detail-commerct.html'
})
export class FoodDetailCommerctPage {
    base_url: any;
    obj: any;
    id: string = "";
    myTax: number;
    settings: any = '';
    categori: number;
    unit: number;
    supplement: number;

    constructor(public navCtrl: NavController,
                public socialSharing: SocialSharing,
                public callNumber: CallNumber,
                public translate: TranslateService,
                public iab: InAppBrowser,
                public navParams: NavParams,
                public http: Http,
                public userService: UserProvider,
                public storage: Storage,
                public currencyProvider: CurrencyProvider,
                public toastCtrl: ToastController,
                public modalCtrl: ModalController,
                private events: Events) {
        this.base_url = Constant.domainConfig.base_url;

    }

    ionViewWillEnter() {


        this.id = this.navParams.get('id');
        let type=this.navParams.get('type');
        this.http.get(Constant.domainConfig.base_url + 'api/foods_api/infoFoods?id=' + this.id +'&type='+type+ '&token=' + this.userService.getUser().token).subscribe(data => {
            this.obj = data.json().data;
            let i = 0;
            for (let model of this.obj.category) {
                if (model.checked)
                    this.categori = i;
                i++;
            }
            i = 0;
            for (let model of this.obj.tax) {
                if (model.checked)
                    this.myTax = i;
                i++;
            }
            i = 0;
            for (let model of this.obj.unite) {
                if (model.checked)
                    this.unit = i;
                i++;
            }
            i = 0;
           /* for (let model of this.obj.supplements) {

                if (model.checked)
                    this.supplement = i;
                i++;
            }*/
        })


    }


    modalAddCart(item) {
        // //
        let modal = this.modalCtrl.create('AddCartPage', {
            'food_id': item.id,
            'discount': item.discount,
            'price': item.price
        });
        modal.present();
    }

    updateTax() {


        for (let model of this.obj.tax) {
            model.checked = false

        }

        this.obj.tax[this.myTax].checked = true;
    }

    updateCategori() {
        for (let model of this.obj.category) {
            model.checked = false

        }
        this.obj.category[this.categori].checked = true;
    }

    updateUnite() {
        for (let model of this.obj.unite) {
            model.checked = false

        }
        this.obj.unite[this.unit].checked = true;
    }

    updateSupplement() {
        for (let model of this.obj.supplement) {
            model.checked = false

        }
        this.obj.supplement[this.supplement].checked = true;
    }


    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    send() {
        this.translate.get(['order', 'myProfile', 'history', 'methodePay', 'cart', 'contact']).subscribe(res => {

            let newObj = {data: this.obj, id: this.id, token: this.userService.getUser().token};
            this.http.put(Constant.domainConfig.base_url + 'api/foods_api/infoFoods', newObj).subscribe(data => {
                if (data.json().status) {
                    this.presentToast(res.modif_made)
                }


            });
        });

    }

}
