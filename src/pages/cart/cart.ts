import {Component,ViewChild} from '@angular/core';
import {
    AlertController, IonicPage, NavController, NavParams, ToastController, LoadingController,
    Events, ModalController
} from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import * as Constant from '../../config/constants';
import {Storage} from '@ionic/storage';
// import { SearchPage } from '../search/search';
import {CallNumber} from '@ionic-native/call-number';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {CurrencyProvider} from '../../providers/currency';
import {LivraisonPage} from "../livraison/livraison";
import {LivraisonPointRetraitPage} from "../livraison-point-retrait/livraison-point-retrait";
//import {StoreCardPage} from "../store-card/store-card";
import {RestGlobals} from "../../providers/rest-globals";
import {UserProvider} from "../../providers/user/user";
import {AdministrationProvider} from "../../providers/administration/administration";
import {CategoriesPage} from "../categories/categories";
import {TranslateService} from "@ngx-translate/core";
//import { Stripe } from '@ionic-native/stripe';
import {MyApp} from "../../app/app.component";

declare var Stripe: any;

@IonicPage()
@Component({
    selector: 'page-cart',
    templateUrl: 'cart.html'
})
export class CartPage {
    
    base_url: any;
    total_card: number = 0;
    validatePrice: boolean;
    list: any;
    tax: any;
    ship_fee: any;
    pay: any;
    valide: boolean = false;
    validate: boolean = false;
    settings: any = '';
    loading: any;
    nodata: boolean = false;
    minimumPrice: number=0;
    maximumDistance: number;
    myIdPromo: string = '';
    discount: string = '';
    montant: string = '';
    data: any;
    verif: boolean = false;
    messageStipeError: string;
    errorMessage: string = "";
    reduction: number = 0;
    myPoints: number = 0;
    savedData: any;
    isCode:boolean=false;
    isNote:boolean=false;
stripe:any;
    constructor(public navCtrl: NavController,
                public navParams: NavParams, public alertCtrl: AlertController,
                public http: Http,
                public userService: UserProvider,
                public translate: TranslateService,
                public modalCtrl: ModalController,
                public callNumber: CallNumber,
                public iab: InAppBrowser,
                public events: Events,
                public storage: Storage,
              
                public currencyProvider: CurrencyProvider,
                public loadingCtrl: LoadingController,
                private administrationService: AdministrationProvider,
                public toastCtrl: ToastController) {

        this.currencyProvider;
        this.loading = this.navParams.get("loading");
        this.base_url = Constant.domainConfig.base_url;
        this.pay = new Array();
        this.data=new Array();
     
        if(RestGlobals.data)
       this.data= RestGlobals.data;
      if(typeof(this.navParams.get('myData'))!='undefined'){
    //   RestGlobals.data=this.navParams.get('myData')
        this.savedData = this.navParams.get('myData'); 
        }

    }

    ionViewDidEnter() {


    
        this.discount = RestGlobals.discount;
        this.discount = RestGlobals.discount;
        this.montant = RestGlobals.montant;
        RestGlobals.lastPage = "CartPage";
        if (!this.userService.getUser() || typeof (this.userService.getUser().token) == "undefined") {

            //this.events.publish("user: login");
            RestGlobals.lastPage = "CartPage";
            this.navCtrl.setRoot('LoginPage');
            return;
        }

        this.storage.get("settings").then(data => {
            this.settings = data;
            this.verif = true;
        });
    
         if(typeof(this.navParams.get('myData'))!='undefined')
         RestGlobals.data = this.navParams.get("myData");

this.chargeData(RestGlobals.data);

    }
chargeData(datt){
RestGlobals.data=datt;
this.data=datt;
 this.savedData = datt;
        if ((RestGlobals.codepormo != '' || RestGlobals.noteCmd != '') && RestGlobals.data) {
            RestGlobals.data.note = RestGlobals.noteCmd;
            RestGlobals.data.code = RestGlobals.codepormo;
            RestGlobals.noteCmd = '';
            RestGlobals.codepormo = '';
        }

        console.log("---------" + JSON.stringify(RestGlobals.data) + "--------------");
        this.orderIsValid();
        this.storage.ready().then(() => {
            this.storage.get('settings').then(data => {
                this.settings = {tax: 0, ship_fee: 0};
            });
            this.storage.get('carts').then((data) => {
                this.list = data;
                this.nodata = true;
                let products_price = 0;
                //  console.log(JSON.stringify(this.list));

                for (var i in this.list) {
if(this.list[i].is_free=='0'){
                    if (this.list[i].is_multiple == '1') {
                        for (let sup of this.list[i].supplements2) {
                            if (sup.checked == true)
                                products_price += parseFloat(sup.price) * parseInt(this.list[i].quantity);
                        }
                    }
                    for (let sup of this.list[i].supplements) {
                        if (sup.checked == true)
                            products_price += parseFloat(sup.price) * parseInt(this.list[i].quantity);
                    }
                    /* if(this.list[i].boisson)  for(let sup of this.list[i].boisson){
                         if(sup.checked)
                             products_price+=parseFloat(sup.price) ;
                     }
                     if(this.list[i].accompagenement) for(let sup of this.list[i].accompagenement){
                         if(sup.checked)
                             products_price+=parseFloat(sup.price) ;
                     }*/
                    products_price = products_price + parseFloat(this.list[i].total_price) * parseInt(this.list[i].quantity);
                }
                }
                this.settings = {tax: 0, ship_fee: 0};

                let frais = 0;
                if (RestGlobals.data && RestGlobals.data.length>0)
                    frais = parseFloat(RestGlobals.data.livraisonObject.frais_livraison) + parseFloat(RestGlobals.data.livraisonObject.frais_service) + parseFloat(RestGlobals.data.livraisonObject.frais_petit_commande);

                this.pay = {
                    products: products_price,
                    tax: parseFloat(this.settings.tax),
                    ship_fee: parseFloat(this.settings.ship_fee),
                    total: parseFloat(this.settings.tax) / 100 * products_price + products_price + parseFloat(this.settings.ship_fee) + this.reduction + frais
                };
this.isFreeProduct(this.pay.total);
                if (this.loading) this.loading.dismiss();

            });
        })
        
        }


    minus(quantity, i) {
        quantity = parseInt(quantity);
        if (quantity - 1 < 1) {
            this.list[i].quantity = 1;
        } else {
            this.list[i].quantity = quantity - 1;
        }
        this.storage.set('carts', this.list);
        this.calc_price();
        this.ValidatePrice();
    }

    plus(quantity, i) {
        quantity = parseInt(quantity);
        if (quantity + 1 > 99) {
            this.list[i].quantity = 99;
        } else {
            this.list[i].quantity = quantity + 1;
        }
        this.storage.set('carts', this.list);
        this.calc_price();
        this.ValidatePrice()
    }

    public back() {
this.navCtrl.setRoot('CategoriesPage');
    }

    public getSafehtml(text) {
        var map = {
            '&amp;': '&',
            '&#038;': "&",
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'",
            '&#8217;': "’",
            '&#8216;': "‘",
            '&#8211;': "–",
            '&#8212;': "—",
            '&#8230;': "…",
            '&#8221;': '”'
        };

        return text.replace(/\&[\w\d\#]{2,5}\;/g, function (m) {
            return map[m];
        });


    }

    enter_quantity(item) {
        item.quantity = parseInt(item.quantity);
        if (item.quantity > 99) {
            item.quantity = 99;
        }
        if (item.quantity < 1) {
            item.quantity = 1;
        }
        this.storage.set('carts', this.list);
        this.calc_price();
    }

    livraison() {
        // this.navCtrl.push('ch');

        let modal = this.modalCtrl.create('CheckoutPage', {
            'total': this.pay.total,
            'livraison': 1,
            'idpormo': this.myIdPromo,
            "data": RestGlobals.data
        });
        modal.present();
         modal.onDidDismiss((data) => {
 if(data && typeof(data)!="undefined"){
 RestGlobals.data=data;
 this.data=data;
 this.chargeData(RestGlobals.data);
      }  
         });

    }

    calc_price() {
        let products_price = 0;

        for (var i in this.list) {
        if(this.list[i].is_free=='0'){
            if (this.list[i].is_multiple == '1') {
                for (let sup of this.list[i].supplements2) {
                    if (sup.checked == true)
                        products_price += parseFloat(sup.price) * parseInt(this.list[i].quantity);
                }
            }
            for (let sup of this.list[i].supplements) {
                if (sup.checked == true)
                    products_price += parseFloat(sup.price) * parseInt(this.list[i].quantity);
            }

            products_price = products_price + parseFloat(this.list[i].total_price) * parseInt(this.list[i].quantity);
        }}


        if (RestGlobals.data && RestGlobals.data.livraisonObject) {
            this.pay = {
                products: products_price,
                tax: parseFloat(this.settings.tax),
                ship_fee: parseFloat(this.settings.ship_fee),
                total: parseFloat(this.settings.tax) / 100 * products_price + products_price + parseFloat(this.settings.ship_fee) + this.reduction + parseFloat(RestGlobals.data.livraisonObject.frais_livraison) + parseFloat(RestGlobals.data.livraisonObject.frais_service) + parseFloat(RestGlobals.data.livraisonObject.frais_petit_commande)
            }
        } else {
            this.pay = {
                products: products_price,
                tax: parseFloat(this.settings.tax),
                ship_fee: parseFloat(this.settings.ship_fee),
                total: parseFloat(this.settings.tax) / 100 * products_price + products_price + parseFloat(this.settings.ship_fee) + this.reduction
            }
        }

this.isFreeProduct(this.pay.total);
    }

isFreeProduct(total){
   var index = -1;
   if(this.list)
for(var i = 0; i < this.list.length; i++) {
    if(this.list[i].is_free == "1") {
        index = i;
        break;
    }
}

if(index>-1){
     this.list.splice(index, 1);
      this.storage.get('carts').then((obj) => {
               
                this.storage.set('carts', this.list);
         });
}


      this.userService
            .findFreeProduct(RestGlobals.idStore)
            .subscribe((data: any) => {
           // data=data.json();
            
                if (data.product_offert && typeof(data.product_offert[1])!="undefined" && data.product_offert[1].etat=="1") {
                 if(parseFloat(data.product_offert[1].montant)<=total){
                  this.list.push(data.product_offert[1].food);
                  console.log(JSON.stringify(this.list));
                   this.storage.ready().then(() => {
            this.storage.get('carts').then((obj) => {
             
                this.storage.set('carts', this.list);
               // this.calc_price();
                });
                 });

                }}
                else if(data.product_offert && typeof(data.product_offert[0])!="undefined" && data.product_offert[0].etat=="1") {
                 
                  if(parseFloat(data.product_offert[0].montant)<=total){
                  this.list.push(data.product_offert[0].food);
                  console.log(JSON.stringify(this.list));
                   this.storage.ready().then(() => {
            this.storage.get('carts').then((obj) => {
             
                this.storage.set('carts', this.list);
             

                });
        });
                }

            }

 

            },
          error => {

        });
}

    remove(index) {
 
        this.storage.ready().then(() => {
            this.storage.get('carts').then((obj) => {
                this.list.splice(index, 1);
                this.storage.set('carts', this.list);
                this.calc_price();
            });
        });


        this.translate.get('delete_obj').subscribe(res => {

            let toast = this.toastCtrl.create({
                message: res.delete_obj,
                duration: 1000,
                position: 'top'
            });

            toast.present();
            return;
        });
    }

    removeAll() {
        this.storage.ready().then(() => {
            this.storage.get('carts').then((obj) => {
                this.list.splice(0, 10000);
                this.storage.set('carts', this.list);
            });
        });
    }

    paiement() {
        let modal = this.modalCtrl.create('CheckoutPage', {
            'total': this.pay.total,
            'livraison': 2,
            'idpormo': this.myIdPromo,
            "data": RestGlobals.data
        });
        //this.navCtrl.push('StoreCardPage');
        modal.present();
         modal.onDidDismiss((data) => {
 
 if(data && typeof(data)!="undefined"){
 RestGlobals.data=data;
 this.data=data;
 this.chargeData(RestGlobals.data);
       } 
         });
    }

    note() {
        let alert = this.alertCtrl.create({
            title: 'Votre note',
            inputs: [
                {
                    name: 'note',
                    type: 'text',
                    placeholder: 'Votre note'
                }
            ],
            buttons: [
                {
                    text: 'Valider',
                    handler: data => {

                        if (!RestGlobals.data){
                            RestGlobals.noteCmd = data.note;
                            this.isNote=true;
                        }
                        else
                            RestGlobals.data.note = data.note;
                    }
                },
                {
                    text: 'x',
                    cssClass: 'cancel',
                    role: 'cancel',
                    handler: data => {

                    }
                }

            ], cssClass: 'promoCode', enableBackdropDismiss: false
        });
        alert.present();
    }

    promo() {
        let alert = this.alertCtrl.create({
            title: 'Code promo',
            inputs: [
                {
                    name: 'codePromo',
                    type: 'text',
                    placeholder: 'Votre code promo'
                }
            ],
            buttons: [
                {
                    text: 'Valider',
                    handler: data => {

                        let headers: Headers = new Headers({
                            "Content-Type": "application/x-www-form-urlencoded"
                        });


                        this.http
                            .get(Constant.domainConfig.base_url + "api/orders_api/check_code_promos?token=" + this.userService.getUser().token + "&code=" + data.codePromo + "&storeId=" + RestGlobals.idStore, {
                                headers: headers
                            })
                            .subscribe(
                                (data: any) => {
                                    this.wsSuccessCode(data.json());
                                },
                                error => {

                                }
                            );
                    }
                },
                {
                    text: 'x',
                    cssClass: 'cancel',
                    role: 'cancel',
                    handler: data => {

                    }
                }

            ], cssClass: 'promoCode', enableBackdropDismiss: false
        });
        alert.present();
    }

    wsSuccessCode(data) {
        this.montant = "";
        this.discount = "";
        if (data.code_promos.status) {

            if (data.code_promos.id) {
                this.myIdPromo = data.code_promos.id;
                if (data.code_promos.pourcentage != null) {
                    this.discount = data.code_promos.pourcentage;
                    RestGlobals.discount = this.discount;
                } else {
                    this.montant = data.code_promos.montant;
                    RestGlobals.montant = this.montant;

                }
                if (!RestGlobals.data){
                    RestGlobals.codepormo = data.code_promos.id;
                    this.isCode=true;
                }
                else
                    RestGlobals.data.code = data.code_promos.id;
            }
        } else {
            let alert = this.alertCtrl.create({
                message: 'Votre code promo est déjà utilisé ou invalide',
                buttons: [{
                    text: 'Ok',
                    role: 'cancel',
                    handler: data => {
                    }
                }]
            })
            alert.present();
        }
    }

    orderIsValid() {


        this.administrationService
            .checkZoneChalendise(this.userService.getUser().token, RestGlobals.idStore)
            .subscribe((data: any) => {
                if (data.status) {
                    if (RestGlobals.data && RestGlobals.data.length>0)
                        RestGlobals.data.reduction = parseFloat(data.data.setting_zone.reduction);
                    this.myPoints = parseInt(data.data.setting_zone.point);
                    this.reduction = parseFloat(data.data.setting_zone.reduction);
                    if (data.data.setting_zone.hasZone) {
                        this.maximumDistance = data.data.setting_zone.zone;

                    }

                } else {
                    RestGlobals.lastPage = "CartPage";
                    this.navCtrl.setRoot(MyApp);
                }


            });

        this.administrationService
            .checkMontant(this.userService.getUser().token, RestGlobals.idStore)
            .subscribe((data: any) => {
                if (data.status && data.data.setting_montant.hasMontant) {
                    this.minimumPrice = data.data.setting_montant.montant;
                    this.ValidatePrice();
                }

            });
    }

    ValidatePrice(): boolean {
alert(this.minimumPrice +" "+ this.pay.total);
        if (this.minimumPrice < this.pay.total || !this.minimumPrice) {
            this.validatePrice = true;
            return true;
        }
        this.validatePrice = false;
        return false;
    }

    checkout() {
        let validationPrice = false;

        if ( RestGlobals.data && typeof (RestGlobals.data.address) != "undefined" && RestGlobals.data.address != null && (RestGlobals.data.type_Paiement != null) &&
            RestGlobals.data.type != null) {

            if ((RestGlobals.data.address == "" ) && RestGlobals.data.type == "livraison") {
                this.validate = true;
                return;
            }
            if(RestGlobals.data.type == "livraison"&&!this.ValidatePrice()){
            return;
            }
            if (RestGlobals.data.type == "stuart" && RestGlobals.data.type_Paiement == "livraison") {
                this.validate = true;
                return;
            }
            validationPrice = true;

            if (this.discount == '' && this.montant == '') RestGlobals.data.total = this.pay.total - this.reduction;
            else if (this.discount == '' && this.montant == '') RestGlobals.data.total = this.pay.total - this.reduction;
            else if (this.discount != '') {
                RestGlobals.data.total = this.pay.total - ((this.pay.total * parseFloat(this.discount)) / 100 + this.reduction);
                RestGlobals.data.point = (this.pay.total * parseFloat(this.discount)) / 100;
            } else
                RestGlobals.data.total = this.pay.total;
            this.validate = false;
        } else {
            this.validate = true;
            return;
        }
        /* let found = RestGlobals.data.forEach(function(element) {
             return typeof (element)=="undefined";
         });
         alert(found);*/
        this.translate.get(['loadingC']).subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.loadingC
            });
        });
        this.loading.present();
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
 this.http
            .post(Constant.domainConfig.base_url + "api/orders_api/add_order", JSON.stringify(RestGlobals.data), {
                headers: headers
            })
            .subscribe(
                (data: any) => {
                    this.wsSuccess(data.json());
                },
                error => {
                    this.loading.dismiss();
                }
            );
    }


    handleAction(response) {
        let that = this;
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
 
 try{
 this.stripe= Stripe(Constant.stripe_publish_key,{'stripeAccount':RestGlobals.stripeAccount});
 }catch(e){alert(e);}
        this.stripe.handleCardAction(
            response.payment_intent_client_secret
        ).then(function (result) {
            if (result.error) {
                that.translate.get(['err_pay', 'ok']).subscribe(res => {
                    let actionSheet = that.alertCtrl.create({
                        title: res.err_pay,
                        buttons: [
                            {
                                text: res.ok,
                                role: 'cancel'
                            }
                        ], enableBackdropDismiss: false
                    });
                    actionSheet.present();
                });
            } else {
                // The card action has been handled
                // The PaymentIntent can be confirmed again on the server
                that.data.payment_method = result.paymentIntent.id;
                 
                that.http
                    .post(Constant.domainConfig.base_url + "api/orders_api/add_order", that.data, {
                        headers: headers
                    })
                    .subscribe(
                        (data: any) => {
                            that.wsSuccess(data.json());
                        },
                        error => {
                            that.loading.dismiss();
                        }
                    );

            }
        });
    }

    wsSuccess(data) {


        if (data) {

            this.messageStipeError = null;
            // console.log((data.requires_action));
            if (data.requires_action == null) {//||data.payment_stripe_return.success
                this.translate.get(['err_ordering', 'success_ordering', 'ok', 'settings']).subscribe(res => {

                    //console.log(data);
                    if (data.ok == false) {
                        let alert = this.alertCtrl.create({
                            message: res.err_ordering
                        });
                        alert.present();
                        if (this.loading) this.loading.dismiss();
                    } else {
                        this.storage.set("carts", new Array());
                        RestGlobals.montant = '';
                        RestGlobals.discount = '';
                        let alert = this.alertCtrl.create({
                            message:
                            res.success_ordering,
                            buttons: [
                                {
                                    text: res.ok,
                                    role: "cancel",
                                    handler: data => {
                                        //this.navCtrl.pop();
                                        this.navCtrl.setRoot('searchStorePage');
                                    }
                                }
                            ]
                        });
                        if (this.loading) this.loading.dismiss();
                        alert.present();
                    }

                });
            } else if (!data.requires_action) {
                this.errorMessage = data.stripe_message_error;
            } else if (data.requires_action == true) {
                // Use Stripe.js to handle required card action
                // if(data.automatic)
                this.handleAction(data);
                /*else if((this.counter+1)<Shared.panierList.length)
                {this.counter++;
                    this.commandeApi.setCommande(RestGlobals.token, [Shared.panierList[(this.counter)]], "test")
                        .subscribe(
                            data => this.wsSuccess(data),
                            error => this.wsFailure(<any>error));
                }*/

            } else {
                this.handleAction(data);
            }
        }

    }

    openSearchPage() {
        this.navCtrl.setRoot('SearchPage');
    }

    facebook() {
        let browser = this.iab.create(this.settings.facebook, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    twitter() {
        let browser = this.iab.create(this.settings.twitter, '_blank', {location: 'yes', toolbar: 'yes'});
    }

    call() {
        this.callNumber.callNumber(this.settings.phone, true);
    }

    commander() {
        this.translate.get('loading_data').subscribe(res => {
            this.loading = this.loadingCtrl.create({
                content: res.loading_data
            });
        });
        this.loading.present();
        this.navCtrl.setRoot('searchStorePage', {loading: this.loading});
    }
}
