import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import  * as Constant from '../../config/constants';
import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {TranslateService} from "@ngx-translate/core";
import {UserProvider} from "../../providers/user/user";

@IonicPage()
@Component({
  selector: 'page-categories-commerct',
  templateUrl: 'categories-commerct.html'
})
export class CategoriesCommerctPage {
  list: any;
    listInitial:any;
  base_url: any;
  total_card: any=0;
  settings:any='';
  loading:any;
    isSerch:boolean=false;
  previousLoading: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http, 
    public callNumber:CallNumber,
    public loadingCtrl: LoadingController,
    public translate:TranslateService,
    public iab: InAppBrowser,
    public userService: UserProvider,
    public toastCtrl:ToastController,
    public storage: Storage ) {
    this.base_url = Constant.domainConfig.base_url;
      this.previousLoading = this.navParams.get("loading");
      this.http.get(Constant.domainConfig.base_url+'api/categories_api/categories?store_id='+this.userService.getUser().store_id).subscribe(data=>{
      this.list = data.json();
     this.listInitial=new Array();
     this.listInitial=data.json();

    });


    this.storage.ready().then(()=>{
      this.storage.get('settings').then(data=>{
        this.settings=data;
      })
    })
  }

    ionClear() {
        this.list = JSON.parse(JSON.stringify(this.listInitial));
    }
  ionViewWillEnter(){
    this.storage.get('carts').then((obj)=>{
    if(obj)  this.total_card = obj.length;
      if(this.previousLoading){
        this.previousLoading.dismiss();
      }
    });
  }

  openPage(item) {
      this.translate.get(['loading_data','no_product']).subscribe(res => {
          if(item.total> 0){
      this.loading = this.loadingCtrl.create({
        content: res.loading_data
      });
      this.loading.present();
      this.navCtrl.push('FoodListCommerctPage', {id:item.id_cat,name:item.name_cat, loading:this.loading});
    }else {
      let toast = this.toastCtrl.create({
        message:res.no_product,
        duration:1000,
        position:'buttom'
      });
      toast.present();
          }
      });
  }

  openCartPage(){
      this.translate.get(['loading_data']).subscribe(res => {
          this.loading = this.loadingCtrl.create({
      content: res.loading_data
    });
    });
    this.loading.present();
    this.navCtrl.setRoot('CartPage',{loading:this.loading});
  }

    search() {
        this.isSerch = !this.isSerch;
    }

    searchBook(searchbar) {
        // reset countries list with initial call
        this.list = JSON.parse(JSON.stringify(this.listInitial));
        var q = searchbar.target.value;

        // if the value is an empty string don't filter the items
        if (q.trim() == '') {
            return;
        }

        this.list = this.list.filter((v) => {
            console.log("vvv " + JSON.stringify(v));
            //   return (v.toLowerCase().indexOf(q.toLowerCase()) > -1)
            return (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1)
        });

    }
  facebook(){
    let  browser = this.iab.create(this.settings.facebook);
  }

  twitter(){
    let  browser = this.iab.create(this.settings.twitter);
  }

  call(){
    this.callNumber.callNumber(this.settings.phone,true);
  }

}
