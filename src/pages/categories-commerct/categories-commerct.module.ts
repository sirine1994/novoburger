import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesCommerctPage } from './categories-commerct';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CategoriesCommerctPage
  ],
  imports: [
    IonicPageModule.forChild(CategoriesCommerctPage),
    TranslateModule.forChild()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CategoriesCommerctPageModule {

}
