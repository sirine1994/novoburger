import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, ToastController, ViewController} from 'ionic-angular';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import  * as Constant from '../../config/constants';

import { Http, Headers } from '@angular/http';

import {EmailValidator} from '../../validators/email';
import {PhoneValidator} from '../../validators/phone';
import {UserNameValidator} from '../../validators/username';
import {PasswordValidator} from '../../validators/password';
import {TranslateService} from "@ngx-translate/core";

@IonicPage()
@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html'
})
export class SignupPage {
    civ: string="";

    base_url: any='';
    form:FormGroup;
iserror:boolean=false;


    constructor(public http:Http, public toastCtrl:ToastController,public viewCtrl:ViewController,public translate:TranslateService, public navCtrl: NavController, public alertCtrl:AlertController, public formBuilder: FormBuilder, public navParams: NavParams) {
        this.base_url=Constant.domainConfig.base_url;
 let datauser = {email:"",name:""};
 if(this.navParams.get("dataUser"))
 datauser=this.navParams.get("dataUser");
  
        this.form = formBuilder.group({
           full_name: ["",Validators.compose([Validators.minLength(5),Validators.maxLength(50),Validators.pattern('[a-zA-Z ]*'),Validators.required])],
            user_name: [datauser.name,Validators.compose([UserNameValidator.isValid,Validators.minLength(5),Validators.maxLength(50)])],
            email: [datauser.email,Validators.compose([EmailValidator.isValid,Validators.required])],
            civ: [null],

            phone:['',Validators.compose([Validators.required, Validators.pattern('[0-9]*'),Validators.minLength(10),Validators.maxLength(10)])],

         //  address:['',Validators.compose([Validators.minLength(5),Validators.maxLength(200),Validators.required])],
            pwd:['',Validators.compose([Validators.required,Validators.minLength(5),Validators.maxLength(50)])],
            repwd:['',PasswordValidator.isMatch],
            send_code_method:['1']
        });
    }

    goBack(){
        this.viewCtrl.dismiss();
    }
    signup(){
         if(!this.form.valid){
        this.iserror=true;
        return;
      }else
       this.iserror=false;
       let signup_url='';
        if(this.form.value.send_code_method==0){
            //if SMS method
            signup_url=this.base_url+'api/users_api/check_sms_register_valid';
        }else{
            //if mail method
            signup_url=this.base_url+'api/users_api/register';
        }

        let headers:Headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let data='user_name='+this.form.value.user_name+
            '&full_name='+this.form.value.full_name+
            '&sexe='+this.civ+
            '&email='+this.form.value.email+
            //'&address='+this.form.value.address+
            '&pwd='+this.form.value.pwd+
            '&phone='+this.form.value.phone;

        // alert(data);

        this.http.post(signup_url, data, {headers: headers}).subscribe(data => {
            // //
            this.translate.get(['plz_verif_code','mail_exist','phone_exist','verif_code','annuler','ok','resend','submit','success_subscribe','err_code_conf']).subscribe(res => {

                let jsonData = data.json();
                if (jsonData.success == 3) {
                    //if success go to verification page
                    let alertCtrl = this.alertCtrl.create({
                        title: res.plz_verif_code,
                        enableBackdropDismiss: false,
                        inputs: [{
                            name: 'code',
                            placeholder: res.verif_code
                        }],
                        buttons: [{
                            text: res.annuler,
                            handler: data => {
                                let post_data = 'email=' + this.form.value.email;
                                this.http.post(this.base_url + 'api/users_api/cancel_register', post_data, {headers: headers}).subscribe(data => {
                                    this.navCtrl.pop();
                                })
                            }
                        },
                            {
                                text: res.resend,
                                handler: data => {
                                    let post_data = 'email=' + this.form.value.email + '&phone=' + this.form.value.phone + '&send_code_method=' + this.form.value.send_code_method;
                                    this.http.post(this.base_url + 'api/users_api/resend_verified_code', post_data, {headers: headers}).subscribe(data => {

                                    })
                                }
                            },
                            {
                                text: res.submit,
                                handler: data => {
                                    let post_data = 'code=' + data.code + '&email=' + this.form.value.email;
                                    this.http.post(this.base_url + 'api/users_api/register', post_data, {headers: headers}).subscribe(data => {
                                        if (data.json().success == 1) {
                                            //register done
                                            let confirmCtl = this.alertCtrl.create({
                                                title:'<img src="assets/img/success.png"/><br/>Succès',
                                                message: res.success_subscribe,
                                                buttons: [{
                                                    text: res.ok,
                                                    handler: () => {
                                                        alertCtrl.dismiss();
                                                        this.navCtrl.pop();
                                                    }
                                                }],cssClass: 'successAlert'
                                            }  );
                                            confirmCtl.present();
                                        } else {
                                            //register failed
                                            let toastCtrl = this.toastCtrl.create({
                                                message: res.err_code_conf,
                                                duration: 3000,
                                                position: 'top'
                                            })
                                            toastCtrl.present();
                                        }//end if else
                                    });
                                    return false;
                                }
                            }]
                    });
                    alertCtrl.present();
                }
                // //
                // //
                if (jsonData.success == 1) {
                    let alertCtrl = this.alertCtrl.create({
                         title:'<img src="assets/img/success.png"/><br/>Succès',
                         message: res.success_subscribe,
                        buttons: [res.ok],cssClass: 'successAlert'
                    })
                    alertCtrl.present();
                    this.navCtrl.setRoot('LoginPage');
                }

                if (jsonData.success == 0) {
                    let alertCtrl = this.alertCtrl.create({
                        title:'<img src="assets/img/success.png"/><br/>Succès',
                         message: res.mail_exist,
                        buttons: [res.ok],cssClass: 'successAlert'
                    })
                    alertCtrl.present();
                }

                if (jsonData.success == 2) {
                    let alertCtrl = this.alertCtrl.create({
                         title:'<img src="assets/img/success.png"/><br/>Succès',
                         message: res.phone_exist,
                        buttons: [res.ok],cssClass: 'successAlert'
                    });
                    alertCtrl.present();
                }
            }, error => {
                this.translate.get(['ok','user_exist']).subscribe(res => {

                    let alertCtrl = this.alertCtrl.create({
                        message: res.user_exist,
                        buttons: [res.ok]
                    });

                    alertCtrl.present();
                    // //
                });
            });
        });
    }
}
