export class CodePromo{
    constructor(){
        this.token='';
        this.code = '';
        this.date_from = new Date().toTimeString().split('T')[0]
        this.date_to = new Date().toTimeString().split('T')[0]
    }
    token:string;
    code:string;
    date_from:string;
    date_to:string;
    montant:number;
    reduction:string;
    qte:string;
}